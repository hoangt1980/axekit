// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : appx_synthesis.cpp
// @brief  : input is a verilog RTL output is a synthesised netlist, not
//           mapped to any technology lib and uses internal pnemonics and
//           as such is useless.
//           To demonstrate the appx_synthesis.
//------------------------------------------------------------------------------

#include "appx_synthesis.hpp"

using namespace axekit;

//------------------------------------------------------------------------------
namespace { // these are our program options.
program_options opts;

std::string ifile, ofile = "kakakikikoo.blif";
std::string top;
std::string report = "kakakikikoo.rpt";
auto debug = 1u;
auto max_cut_size = 3;
std::string check_port;
int wc_limit = -1;
int bf_limit = -1;
int er_limit = -1;
int effort = 3;

// Flags to indicate what all needs to be run
// error-rate is always taken in second stage miter. No need to mix.
// wc, bf, and wc_bf flags are mutually exclusive.
bool wc_flag = false;
bool bf_flag = false;
bool er_flag = false;
bool wc_bf_flag = false;

//auto node_ctr = 0u;
int opt_style = 0; // default delay

// for error rate computations.
cirkit::bdd_function_t golden_bdd;
Cudd *cudd_mgr;
boost::multiprecision::uint256_t target_error_count(0u);
}


//------------------------------------------------------------------------------
// Some helper functions moved out of main.

int parse_and_check_options (int argc, char **argv) {
  using boost::program_options::value;
  
  opts.add_options() 
    ( "ifile,i",   value ( &ifile ),  "Input Verilog RTL" )
    ( "ofile,o",   value_with_default ( &ofile ),  "Output BLIF" )
    ( "top,t",     value ( &top ),    "Name of the TOP Module" )
    ( "check_port,p",   value ( &check_port ), "Port to Check" )
    ( "worst_case,w",   value_with_default ( &wc_limit ),   "Worst Case Limit, -ve value ignores" )
    ( "bit_flip,b",     value_with_default ( &bf_limit ),   "Bit Flip Limit, -ve value ignores" )
    ( "error_rate,e",   value_with_default ( &er_limit ),   "Error Rate Limit (in %), -ve value ignores" )
    ( "report,r",       value_with_default ( &report ), "Output report" )
    ( "debug,d",        value_with_default ( &debug ),  "Extra debugging levels" )
    ( "max_cut_size,m", value_with_default( &max_cut_size ),     "Test Input" )
    ( "opt_style,a",    value_with_default( &opt_style ), "Delay=0/Area=1/Random=2 (default delay)" )
    ( "effort",         value_with_default( &effort ), "Effort level (default maximum or 3)" )
    ;
  
  try {
      opts.parse( argc, argv );
  } 
  catch (std::exception &e) {
    std::cout << "[e] Error in options provided - " << e.what() << "\n";
    std::cout << "[i] Available Options: \n" << opts;
    return -1;
  }
  if ( opts.is_set("help") ) {
    std::cout << opts << std::endl;
    return 0;
  }
  if ( !opts.good() || !opts.is_set("ifile") || !opts.is_set("top") ||  
       !opts.is_set("check_port") ) {
    std::cout << "[e] Error: Bad options!\n";
    std::cout << "[i] Available Options: \n" << opts;
    return -2;
  }
  if (opt_style > 2) {
    std::cout << "[w] Unknown opt style (only 0/1/2 allowed). Defaults to 0 (delay)\n";
    opt_style = 0;
  }
  if (debug > 0) {
    std::cout << "[i] start_time  = " << curr_time();
    std::cout << "[i] ifile       = " << ifile << "\n";
    std::cout << "[i] ofile       = " << ofile << "\n";
    std::cout << "[i] top         = " << top << "\n";
    std::cout << "[i] check_port  = " << check_port << "\n";
    std::cout << "[i] report      = " << report << "\n";
    std::cout << "[i] optimization style     = " << opt_style << "\n";
    std::cout << "[i] effort level           = " << effort   << "   (max level is 3) \n";
    std::cout << "[i] worst case limit       = " << wc_limit << "\n";
    std::cout << "[i] bit flip limit         = " << bf_limit << "\n";
    std::cout << "[i] error rate limit       = " << er_limit << "\n";
  }
  if (debug > 1) {
    std::cout << "[i] max_cut_size = " << max_cut_size << "\n";
    std::cout << "[i] debug_level  = " << debug << "\n";
  }

  return 0; // normal value
}


void check_error_limits (const Port &port_to_check, const Design &design, 
			 const int &po_width) {
  
  if (bf_limit > po_width) {
    bf_limit = po_width;
    std::cout << "[e] Bit Flip Errors larger than the width of port to check!\n";
    std::cout << "[w] bit_flip error limit modified to " << po_width 
	      << " (full freedom), width of port \"" 
	      << get_port_name (port_to_check) << "\"\n";
  }

  // careful wit long nd int, else it might spill out.
  //--wrong--long max_wc_error = (1u << po_width) - 1; 
  //--wrong--assert ( (width < sizeof(max_wc_error) * 8) && "This much big output port width!!" );
  //--wrong--if (wc_limit > max_wc_error) {
  //--wrong--  wc_limit = max_wc_error;
  //--wrong--}

  // check the error rate also, if its ridiculous or not.

  // some special cases.
  // 1. if everything is full freedom, then the best circuit is jst 1 or 0.

}

// TODO : make this proper.
void flag_relevant_miters () {
  bf_flag = false; wc_flag = false; wc_bf_flag = false, er_flag = false;

  if ( bf_limit >= 0 ) bf_flag = true;
  if ( wc_limit >= 0 ) wc_flag = true;

  if (bf_flag && wc_flag) {
    bf_flag = false; wc_flag = false;
    wc_bf_flag = true;
  }
  
  if (er_limit >= 0) er_flag = true;

  // If any error happens, none of these limits should be zero.
  assert ( (0 != bf_limit)  && (bf_flag || wc_flag || wc_bf_flag || er_flag) && "Impossible Target" );
  assert ( (0 != wc_limit)  && (bf_flag || wc_flag || wc_bf_flag || er_flag) && "Impossible Target" );
  assert ( (0 != er_limit)  && (bf_flag || wc_flag || wc_bf_flag || er_flag) && "Impossible Target" );

}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//   MAIN 
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
int main (int argc, char **argv) {
  auto opts_status = parse_and_check_options (argc, argv);
  if (0 != opts_status) return opts_status;
  flag_relevant_miters();
  //-----------------------------------------------------------
  auto max_attempts = 10u; // Maximum cut attempts allowed.
  auto applied_cuts = 0u;  // Tracks number of succesful cuts for approx.
  auto n_top_paths = 5u;   // takes only these many paths for approx.

  const std::string work = "work";
  create_work_dir ( work ); // Create the work directory.
  auto work_path = work + "/"; 
  //-----------------------------------------------------------
  // 1. Get the corresponding blifs.
  auto iblif = work_path + "input.blif";
  auto oblif = ofile; // TODO, for the timebeing.
  start_yosys();
  verilog_to_blif_in_ys (iblif, ifile, top);
  auto width = get_port_width_in_ys ( check_port );
  auto port_to_check = make_port (check_port, width);

  
  // Start the random number generator.
  std::srand ( unsigned  (std::time(0)) );


  //-----------------------------------------------------------
  // 2. Fire up ABC 
  Frame frame  = start_abc();
  Network ntk  = read_blif_to_network (iblif);
  assert ( is_network_good (ntk) );
  auto design = make_design_from_network (ntk); 
  check_error_limits (port_to_check, design, width);

  std::string orig_vlog = work_path + "orig.v";
  if ( !check_network (ntk) )
    std::cout << "[e] Error. Original Network integrity failed..  :("
	      << std::endl;
  //write_blif_from_network (orig_vlog, ntk);
  write_verilog_from_network (orig_vlog, ntk);
  if (debug > 10) print_network_details (ntk, "Original Network :: ");
  auto orig_ntk_name = get_network_name ( ntk );
  auto orig_ntk_node_cnt = get_node_count ( ntk );

  //-----------------------------------------------------------
  init_appx_miter (work_path, debug, orig_vlog); // initialize  Approx-Miter

  //-----------------------------------------------------------
  cudd_mgr = new Cudd();
  if (er_flag) { // get the golden BDD for error-rate
    boost::multiprecision::uint256_t one(1u);
    target_error_count  = ( one << (get_pi_list (ntk)).size() ) * (unsigned)er_limit ;
    target_error_count = target_error_count / 100;
    golden_bdd = network_to_bdd (ntk, *cudd_mgr);
    if (debug > 1) 
      std::cout << "[w] Error-Rate computations can be considerably longer\n";
  }
  //------------------------------------------------------------
  auto attempts = 0u;
  unsigned t_time_sort_paths = 0u, t_time_apply_cut = 0u, t_time_generate_cuts = 0u;

      
  // appx_ntk nt needed, can work wt ntk itself.
  // Copy overhead in initial versions jst to avoid confusion
  auto appx_ntk = copy_network ( ntk ); 
  set_network_name ( appx_ntk, get_network_name (ntk) + "_appx" );
  delete_network ( ntk ); // not needed anymore.
  //std::vector <Object> tried_node_list; // keep track of tried nodes.
  // Storing pointers in vector is a bug.
  std::vector <std::string> tried_node_list; // keep track of tried nodes.
  while (attempts < max_attempts) // The Main approximation Loop.
  {
    if (1 == attempts && 0 == applied_cuts) { // hopeless, not going to improve.
      std::cout << "[w] Stopping approximation here. Cant find any suitable cut in"
		<< " any of the " << n_top_paths << " paths!\n";
      std::cout << "[w] Approximated and Original are same networks!\n";
      break;
    }
    attempts++;
    appx_ntk = strash_network (appx_ntk); // Re-strash 
    assert ( is_topologically_sorted (appx_ntk) );
    // 3. Sort the longest paths.
    if (debug > 1) std::cout << "[i] Sorting the longest paths begin :: " << curr_time();
    auto begin_time = std::clock ();
    if (debug > 10) print_network_details (appx_ntk, "Step " + std::to_string(attempts) + 
					   " : Intermediate network");
    auto po_list = get_po_list (appx_ntk);
    //auto nTop = std::min ( n_top_paths, (unsigned int)po_list.size() );
    auto nTop = (unsigned)po_list.size();  // sort everything. (default max-effort)
    if (2 == effort) nTop = std::min (10u, nTop);
    if (1 == effort) nTop = std::min (5u,  nTop);
    if (0 == effort) nTop = std::min (1u,  nTop);
    std::vector <Path> paths_list;
    for (auto &po : po_list) { // TODO: interested only in nTop paths.
      auto longest_path = get_longest_path_to_po (po);
      // prune const drivers to PO
      if ( is_node_const ( get_pi_of_path (longest_path) ) ) continue;
      paths_list.emplace_back (longest_path);
    }
    if ( 0u == po_list.size()) break; // case where all PO drivers are consts

    sort_paths ( paths_list, nTop);
    auto time_sort_paths = get_elapsed_time ( begin_time );
    t_time_sort_paths += time_sort_paths; // total time
    if (debug > 1) {
      std::cout << "[i] Sorting the longest paths end :: " << curr_time();
      std::cout << "[i] Sorting time = " << time_sort_paths << std::endl;  
    }
    if (debug > 10) std::cout << nTop << " Top Paths in the sorted order \n";
    if (debug > 10) print_paths (paths_list); // Print longest paths in the order.
    //------------------------------------------------------------
    // Merge same length paths first. 
    // This is a distructive call, paths_list will be useless except only for 
    // our algorithm.
    // And there will be duplications in nodes. (use tried_list_nodes for this)
    if (0 == opt_style) {
      merge_same_length_paths (paths_list);
      nTop = (unsigned)paths_list.size();  // after merging size will change.
      if (debug > 10) std::cout << "Paths after merging :: \n";
      if (debug > 10) print_paths (paths_list); // Print longest paths in the order.
    }
    //------------------------------------------------------------
    // CUTS - get the first cut and then exit
    // Get a flattend list of nodes to cut based on 1) longest path & 2) cut-volume.
    // 4. Sort based on cut-volume
    if (debug > 1) std::cout << "[i] Cut generation begin :: " << curr_time();
    begin_time = std::clock();
    auto cut_mgr = start_cut_manager (max_cut_size);
    auto total_nodes = get_node_count ( appx_ntk );
    std::vector <Object> node_list;
    for (auto i = 0u; i < nTop; i++) {
      auto cut_volume_list = get_cut_volume_list ( cut_mgr, paths_list[i] );
      if (0 == opt_style)  // default delay
	reverse_sort_cut_volume_list (cut_volume_list);
      else if (1 == opt_style) // area 
	sort_cut_volume_list ( cut_volume_list );
      else  // pick random cuts.
	randomize_cut_volume_list ( cut_volume_list );
      std::transform ( cut_volume_list.begin(), cut_volume_list.end(),
		       back_inserter(node_list),
		       [] (const CutVolume &cut) {return cut.first;} );
      if (2 == opt_style)  // well, why not double randomize!
	randomize_cut_volume_list ( cut_volume_list );
    }
    if (debug > 1) std::cout << "[i] Cut generation end :: " << curr_time();
    auto time_generate_cuts = get_elapsed_time ( begin_time );
    t_time_generate_cuts += time_generate_cuts; // total time
    if (debug > 1) std::cout << "[i] Cut generation time = " 
			     << time_generate_cuts << std::endl;
    
    // Assuming very fast convergence, it might be better to maintain
    // a list to check if processed rather than attempting to remove
    // the duplicates in unsorted node_list, for successive cuts.
    // TODO: BUG CRASH for redoing a cut on an already cut node. (done?)
    // Note : fast converging assumption is wrong, but still doing the list.
    
    // 6. Do the cutting.
    if (debug > 1) std::cout << "[i] Apply Cut & Solve begin :: " << curr_time();
    begin_time = std::clock();
    std::string cut_node_name;
    auto applied_cuts_before = applied_cuts;
    for (auto &node : node_list) {
      //assert (node_ctr < 4); node_ctr++;
      auto curr_node_name = std::string (get_obj_name(node));
      auto is_node_tried = std::find ( tried_node_list.begin(), tried_node_list.end(), 
				       curr_node_name );
      if ( is_node_tried != tried_node_list.end() ) continue;
      tried_node_list.emplace_back (curr_node_name);
      if ( !is_node_good(node) ) continue;
      if ( !is_node_internal(node) ) continue;
      auto work_ntk = copy_network ( appx_ntk ); // Try first on a work copy.
      // two different networks, this translation is a must.
      auto node_work = get_ith_object ( work_ntk, node->Id );
      if ( node_work == nullptr ) continue; // TODO : remove leak 
      if (debug > 5) std::cout << "[i] Trying node - " << get_obj_name (node_work) 
			       << " for approximation : \n";
      //if (debug > 5 ) print_dbg ("Trying node - " + std::string ( get_obj_name (node_work) ) 
      //				 + " for approximation : " + std::string( curr_time() ) );
      replace_node1_with_node2 (work_ntk, node_work,  get_const1 (work_ntk));
      // create and solve the approximation miters.
      SolverResult results = SolverResult::UNINIT; 
      // One and only of these flags must be true.
      if (wc_flag) results = solve_wc_appx_miter (design, port_to_check, wc_limit, work_ntk);
      if (bf_flag) results = solve_bf_appx_miter (design, port_to_check, bf_limit, work_ntk);
      if (wc_bf_flag) results = solve_wc_bf_appx_miter (design, port_to_check, wc_limit, 
							bf_limit, work_ntk);

      //------------------------------------------------------------------------
      // Nw need to consider all combinations of err, wc and bf.
      // case 1, only err
      if ( results == SolverResult::UNINIT ) { // no wc and bf
	auto approx_bdd = network_to_bdd (work_ntk, *cudd_mgr);
	auto error_count = cirkit::error_rate (golden_bdd, approx_bdd);
	delete_network ( work_ntk );  // dont need anymore
	
	if (error_count < target_error_count) {
	  // Replay and there is further scope for cutting.
	  if (debug > 1) std::cout << "[i] Cutting Node " << get_obj_name (node)
				   <<  " - successful, keeping the current cut [err = " 
				   << error_count << "]\n";
	  //cut_node_name = get_obj_name (node); // after update names change, hence meaningless.
	  replace_node1_with_node2 ( appx_ntk, node, get_const1 (appx_ntk) );
	  applied_cuts++;
	  break; // Need to recompute everything. After replacement, nix holds.
	}
	else {
	  if (debug > 1) std::cout << "[i] Cutting Node " << get_obj_name (node)
				   <<  " - unsuccessful, skipping the current cut\n";
	}
      } // case 1 ends
      // case 2 wc/bf/er combinations.
      else {
	if ( results == SolverResult::NO ) {
	  if (er_flag) { // 2.1. wc/bf + er
	    auto approx_bdd = network_to_bdd (work_ntk, *cudd_mgr);
	    auto error_count = cirkit::error_rate (golden_bdd, approx_bdd);
	    delete_network ( work_ntk );  // dont need anymore
	    if (error_count < target_error_count) {
	      // Replay and there is further scope for cutting.
	      if (debug > 1) std::cout << "[i] Cutting Node " << get_obj_name (node)
				       <<  " - successful, keeping the current cut [er = " 
				       << error_count << "]\n";
	      replace_node1_with_node2 ( appx_ntk, node, get_const1 (appx_ntk) );
	      applied_cuts++;
	      break; // Need to recompute everything. After replacement, nix holds.
	    }
	    else {
	      if (debug > 1) std::cout << "[i] Cutting Node " << get_obj_name (node)
				       <<  " - unsuccessful, skipping the current cut\n";
	    }
	  } 
	  else { // 2.2. only wc/bf
	    delete_network ( work_ntk );  // dont need anymore
	    // Replay and there is further scope for cutting.
	    if (debug > 1) std::cout << "[i] Cutting Node " << get_obj_name (node)
				     <<  " - successful, keeping the current cut\n";
	    //cut_node_name = get_obj_name (node); // after update names change, hence meaningless.
	    replace_node1_with_node2 ( appx_ntk, node, get_const1 (appx_ntk) );
	    applied_cuts++;
	    break; // Need to recompute everything. After replacement, nix holds.
	  }
	} // ! results == SolverResult::NO
	else { // 2.3. All unsuccessful. Proceed with nxt iteration.
	  delete_network ( work_ntk );  // dont need anymore
	  if (debug > 1) std::cout << "[i] Cutting Node " << get_obj_name (node)
				   <<  " - unsuccessful, skipping the current cut\n";
	}
      } // case 2 ends
      //------------------------------------------------------------------------
    } // for (auto &node : node_list)
    
    
    auto time_apply_cut = get_elapsed_time ( begin_time );
    t_time_apply_cut += time_apply_cut; // total time
    if (debug > 1) {
      std::cout << "[i] Apply Cut & Solve end :: " << curr_time();
      std::cout << "[i] Cut Apply time = " << time_apply_cut << std::endl;
    }
    if ( applied_cuts_before == applied_cuts ) { // not going to improve.
      if (debug > 1) 
	std::cout << "[i] Stopping approximation. Cannot improve further.\n";
      break;
    }
  } // while (attempts < max_attempts)

  //------------------------------------------------------------  
  if (debug > 10) print_network_details (appx_ntk, "Approximated Network :: ");
  // 7. Check the network integrity & dump the networks.
  if ( !check_network (appx_ntk) )
    std::cout << "[e] Error. Approximated Network integrity failed..  :("
	      << std::endl;

  write_blif_from_network ( oblif, appx_ntk );
  std::ofstream rpt;
  rpt.open ( report );
  rpt << std::fixed << std::showpoint << std::setprecision(2);
  rpt << "Maniac Approximate Synthesis report " << curr_time();
  rpt << "------------------------------------------------------------\n";
  rpt << "Orig circuit         :: " << orig_ntk_name << std::endl;
  rpt << "Orig circuit nodes   :: " << orig_ntk_node_cnt << std::endl;
  rpt << "Orig circuit file    :: " << ifile << std::endl;
  rpt << "Approx circuit       :: " << get_network_name ( appx_ntk ) << std::endl;
  rpt << "Approx circuit nodes :: " << get_node_count ( appx_ntk ) << std::endl;
  rpt << "Approx circuit file  :: " << ofile << std::endl;

  rpt << "Checked Port         :: " << check_port << std::endl;
  rpt << "Top Module           :: " << top << std::endl;
  rpt << "Worst Case limit     :: " << wc_limit << std::endl;
  rpt << "Bit Flip limit       :: " << bf_limit << std::endl;
  rpt << "Error Rate limit (%) :: " << er_limit << std::endl;
  rpt << "Optimization Style   :: " << opt_style << "   (0=delay,  1=area, 2=random)\n";
  rpt << "Effort level         :: " << effort    << "   (max level is 3) \n";


  //rpt << "Longest Path      :: " << get_path_string (paths_list.at(0)) << std::endl;
  //rpt << "Cut Node                 :: " << cut_node_name << std::endl; 
  rpt << "Path Sorting Time (s)    :: " << t_time_sort_paths << std::endl;
  rpt << "Cut Apply Time (s)       :: " << t_time_apply_cut << std::endl;
  rpt << "Cut Enumeration Time (s) :: " << t_time_generate_cuts << std::endl;
  rpt << "Total Time (s)           :: "
      << (t_time_sort_paths + t_time_apply_cut + t_time_generate_cuts) << std::endl;
  rpt.close();
  delete_network ( appx_ntk );
  stop_abc();
  stop_yosys();
  std::cout << "[i] finish_time = " << curr_time();
  return 0;
}



//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
