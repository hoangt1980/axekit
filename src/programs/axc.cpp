// Copyright (C) AGRA - University of Bremen
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : axc.cpp
// @brief  : The aXc application.
//
//           AxC is a collection of algorithms and programs for synthesis
//           and verification of approximate computing.
//           AxC uses alice command line interface, for basic user interaction.
//           
//------------------------------------------------------------------------------

#include <programs/axc_version.hpp>

#define LIN64

// All IO Commands
#include <commands/axekit_stores.hpp>
#include <commands/io/aiger_io.hpp>
#include <commands/io/verilog_io.hpp>
#include <commands/io/blif_io.hpp>
#include <commands/io/aig_to_bdd.hpp>
#include <commands/io/aig_to_ntk.hpp>
#include <commands/io/ntk_to_aig.hpp>

#include <commands/version.hpp>
#include <commands/report_error.hpp>
#include <commands/report_seq_error.hpp>
#include <commands/total_arithmetic_error.hpp>
#include <commands/arithmetic.hpp>
#include <commands/logical.hpp>

// All synthesis commands
#include <commands/compile/compile.hpp>

// Simulation commands
#include <commands/simulate/simulate.hpp>

// Experimental test feature.
#include <commands/test.hpp>


// MAIN PROGRAM
int main (int argc, char **argv) {
  using namespace alice;
  cli_main <abc::Gia_Man_t*,        // Gia, the latest ABC-AIG datastructure.
	    cirkit::bdd_function_t, // CUDD bdd type.
	    abc::Abc_Ntk_t*,        // Old AIG network.
	    abc::Wlc_Ntk_t*>        // Word level network, for multibit output. Not complete.
    cli( "aXc" );

  
  cli.set_category( "I/O" );
  ADD_COMMAND( read_aiger );
  ADD_COMMAND( read_blif );
  ADD_COMMAND( read_verilog );
  ADD_COMMAND( write_aiger );
  ADD_COMMAND( write_blif );

  cli.set_category( "Store conversion" );
  ADD_COMMAND( aig_to_bdd );
  ADD_COMMAND( aig_to_ntk );
  ADD_COMMAND( ntk_to_aig );

  cli.set_category( "Miscallenous" );
  ADD_COMMAND( version );
  
  cli.set_category( "Reporting" );
  ADD_COMMAND( report_error );
  ADD_COMMAND( report_seq_error );
  ADD_COMMAND( total_arithmetic_error );

  cli.set_category( "Functions" );
  ADD_COMMAND( arithmetic );
  ADD_COMMAND( logical );

  cli.set_category( "Synthesis" );
  ADD_COMMAND( compile );

  cli.set_category( "Simulation" );
  ADD_COMMAND( simulate );
  
  cli.set_category( "Experimental" );
  ADD_COMMAND( test );

  std::cout << banner_string();
  return cli.run( argc, argv );

  
}


// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
