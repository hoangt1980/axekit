// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : common_utils.cpp
// @brief  : Some common utilities.
//------------------------------------------------------------------------------

#include "common_utils.hpp"

// TODO:
// change completely to std::chrono
//       auto t1 = std::chrono::steady_clock::now();
//       auto t2 = std::chrono::steady_clock::now();
//       auto t3 = std::chrono::steady_clock::now();
//       auto t4 = std::chrono::steady_clock::now();
// fprintf (conf.outputFile,"Time taken: %d %d %d \n",
//          axekit::diff_time (t1, t2), to_time (t2, t3), to_time (t3, t4) );



namespace axekit
{

//------------------------------------------------------------------------------
mpuint bitset_to_mpuint ( const boost::dynamic_bitset<> &bs ) {
  static const auto ulong_size = sizeof (ulong) * 8;
  if (bs.size() < ulong_size ) return bs.to_ulong();
  mpuint sum(0);
  if ( bs.test(0) ) sum = 1;
  const mpuint two(2);
  for (auto i=1u; i <bs.size(); i++) { // bs does not hav iterators?
    if ( bs.test(i) ) sum = sum + (two << i);
  }
  return sum;
  
}

boost::dynamic_bitset<> mpuint_to_bitset ( const mpuint &num ) {
  boost::dynamic_bitset<> bs;
  const mpuint one(1);
  const mpuint zero(0);

  if (num == one) {
    bs.push_back (true) ; return bs;
  }
  if (num == zero) {
    bs.push_back (false); return bs;
  }
  
  auto val = num;
  while (val > 0) {
    auto bit = val & one;
    val = val >> 1;
    if (bit == one) bs.push_back (true);
    else bs.push_back (false);
    if (val == zero) return bs;
  }
  return bs; 
}



//------------------------------------------------------------------------------
void create_work_dir (const std::string &dir_name) {
  boost::filesystem::path p(dir_name);
  if ( boost::filesystem::exists (p) ) {
    std::cout << "[w] Directory \"" + dir_name + "\" exists. "
	      << "Contents will be overwritten"  << std::endl;
    return;
  }
  auto status = boost::filesystem::create_directory (p);
  if (!status)  std::cout << "[e] Cannot create work directory "
			  << "(Permissions? Disk full?)" << std::endl;
  assert (status);
}

void delete_work_dir (const std::string &dir_name) {
  boost::filesystem::path p(dir_name);
  boost::filesystem::remove_all (p);
}

char *  curr_time () {
  time_t now = time(0);
  return ctime(&now);
}

float get_elapsed_time ( const clock_t &begin_time ) {
  return float ( std::clock() - begin_time ) / CLOCKS_PER_SEC;
}

void print_dbg (const std::string &str) {
  // Nxt time use anonymous namespace; not static
  static unsigned counter = 0;
  std::cout << counter << "." << str << std::endl;
  counter++;
}

void cat_two_files ( const std::string &ofile, const std::string &ifile1, 
		     const std::string &ifile2 ) {
  std::ifstream ifs1 (ifile1);
  std::ifstream ifs2 (ifile2);
  
  std::string data1 ( ( std::istreambuf_iterator<char>(ifs1) ),
		      std::istreambuf_iterator<char>() );
  std::string data2 ( ( std::istreambuf_iterator<char>(ifs2) ),
		      std::istreambuf_iterator<char>() );
  ifs1.close();
  ifs2.close();

  std::ofstream ofs (ofile);
  ofs << data1;
  ofs << data2;
  ofs.close();

}

void cat_three_files ( const std::string &ofile, const std::string &ifile1, 
		       const std::string &ifile2, const std::string &ifile3 ) {

  std::ifstream ifs1 (ifile1);
  std::ifstream ifs2 (ifile2);
  std::ifstream ifs3 (ifile3);
  
  std::string data1 ( ( std::istreambuf_iterator<char>(ifs1) ),
		      std::istreambuf_iterator<char>() );
  std::string data2 ( ( std::istreambuf_iterator<char>(ifs2) ),
		      std::istreambuf_iterator<char>() );
  std::string data3 ( ( std::istreambuf_iterator<char>(ifs3) ),
		      std::istreambuf_iterator<char>() );
  ifs1.close();
  ifs2.close();
  ifs3.close();

  std::ofstream ofs (ofile);
  ofs << data1;
  ofs << data2;
  ofs << data3;
  ofs.close();

}

std::string get_name_of_file (const std::string &file_name) {
  boost::filesystem::path p(file_name);
  return p.stem().string();
}

std::string get_extension_of_file (const std::string &file_name) {
  boost::filesystem::path p(file_name);
  return p.extension().string();
}

//------------------------------------------------------------------------------
}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
