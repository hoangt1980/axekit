// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : mc_worst_case.cpp
// @brief  : Maniac procedures for sequential verification.
//------------------------------------------------------------------------------

#include "mc_worst_case.hpp"


namespace maniac
{
/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/
//------------------------------------------------------------------------------
inline std::string pdr_result_to_string (const unsigned short result) {
  if (!result) return " NO ";
  if (result) return " YES ";
  return " ERROR - IMPOSSIBLE CONDITION ";
  
}

inline std::string pdr_result_to_string_orig (const unsigned short result) {
  if (result == 3u) return " NO ";
  if (result == 2u) return " YES ";
  if (result == 1u) return " UNDECIDED ";
  return " ERROR - IMPOSSIBLE CONDITION ";
  
}

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

unsigned long mc_worst_case (const mc_design &golden, const mc_design &approx,
			     const mc_port &port_to_check, unsigned signed_outputs,
			     std::string work, bool debug, const unsigned &opt)
{
  const unsigned short NO = 3u, YES = 2u, UNDECIDED = 1u;
  
  auto golden_vlog = work + "/golden.v";
  auto approx_vlog = work + "/approx.v";
  golden.write ( golden_vlog, "mc_golden", "verilog", opt );
  approx.write ( approx_vlog, "mc_approx", "verilog", opt );

  auto max_iterations = 200u;

  auto lower_bound = 0u;
  auto curr_error = (1ul <<  port_to_check.second) - 1;
  auto upper_bound = curr_error;

  if (debug) std::cout << "[i] BinSearch PDR on output :: " << port_to_check.first
		       << "  [width = " << port_to_check.second << "]" << std::endl;
  // TODO: ABC may need to be start/stop cycled for memory buildup.
  // Binary Search.
  auto step = 1u;
  while (lower_bound < upper_bound)
  {
    std::string miter_vlog =  work + "/miter" + std::to_string(step) + ".v";
    std::string miter_blif =  work + "/miter" + std::to_string(step) + ".blif";
    std::string miter_cex  =  work + "/miter" + std::to_string(step) + ".cex";
    std::string miter_summary =  work + "/miter" + std::to_string(step) + ".summary";
    std::string miter_snippet = work + "/m" + std::to_string(step) + ".v";
    
    auto sum = upper_bound + lower_bound;
    auto lsb = (sum & 1u);
    curr_error = lsb ? ( (sum >> 1) + 1 ) : (sum >> 1) ; // (ceil (u+l)/2)
    //std::cout << "sum = " << sum << "  ::  lsb = " << lsb << std::endl;
    //curr_error = (sum >> 1u) | lsb; // (ceil (u+l)/2)

    // 1. Create the MITER.
    // a. create the miter snippet.
    if (debug) std::cout << "[i] Step " << step << " Started  creating max-error miter :: "
			 << curr_time();
    mc_make_miter ( miter_snippet, golden, approx, "max_error_miter", port_to_check,
		    curr_error, signed_outputs, mc_miter_type::type2 );
    // b. cat golden.v + approx.v + miter.v
    axekit::cat_three_files ( miter_vlog, golden_vlog, approx_vlog, miter_snippet );
    auto miter = mc_design ( miter_vlog, "max_error_miter", "clock",
			     golden.get_reset(), golden.get_oe(), false  );
    miter.write (miter_blif, "blif", opt);
    if (debug) std::cout << "[i] Step " << step << " Finished creating max-error miter :: "
			 << curr_time();

    // 2. Run PDR.
    if (debug) std::cout << "[i] Step " << step << " Starting PDR :: BinSearch bounds ["
			 << lower_bound << "," << upper_bound
			 << "] :: " << port_to_check.first << " >= "
			 << curr_error << "?   :: " << curr_time();
    //std::cout << "ORIG PDR ---------------------------------------------\n";
    //auto result = mc_pdr_orig (miter_blif, miter_cex, miter_summary, debug);
    //std::cout << "AXE PDR ---------------------------------------------\n";
    auto result = mc_pdr (miter_blif, miter_cex, miter_summary, debug);
    //if (result) std::cout << "Axe PDR result = true\n\n" ;
    if (debug) std::cout << "[i] Step " << step << " Finished PDR :: BinSearch bounds ["
			 << lower_bound << "," << upper_bound
			 << "] :: " << port_to_check.first << " >= "
			 << curr_error << "?   :: " << pdr_result_to_string (result)
			 << "  :: " << curr_time();

    //--orig--// 3. Adjust the bounds
    //--orig--if (result == NO) { // lower_bound remains the same.
    //--orig--  upper_bound = curr_error - 1; // since we are verifying >= 
    //--orig--}
    //--orig--else if (result == YES) { // upper bound remains the same.
    //--orig--  lower_bound = curr_error;
    //--orig--}
    //--orig--else if (result == UNDECIDED) { // Cant figure out by this method.
    //--orig--  std::cout << "[e] Inference :: worst-case is between " << lower_bound
    //--orig--		<< " and " << upper_bound << std::endl;
    //--orig--  std::cout << "[e] Cannot process further. Returning 0 " << std::endl;
    //--orig--  return 0u;
    //--orig--}

    // 3. Adjust the bounds
    if (!result) { // lower_bound remains the same.
      upper_bound = curr_error - 1; // since we are verifying >= 
    }
    else if (result) { // upper bound remains the same.
      lower_bound = curr_error;
    }
    
    if (step >= max_iterations) {
      std::cout << "[e] Inference :: worst-case is between " << lower_bound
    		<< " and " << upper_bound << std::endl;
      std::cout << "[e] Aborting after " << step << "th step. Returning 0 "
    		<< std::endl;
      return 0u;
    }
    step++;
    
  } // while loop
  
  assert (lower_bound == upper_bound); // Never fails.
  return lower_bound;
  
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
