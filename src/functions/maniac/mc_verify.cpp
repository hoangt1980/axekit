// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
/**
 * @file mc_verify.cpp
 *
 * @brief Formal verifier for Sequential Approximate ckts.
 *
 * @author Arun
 */

#include "mc_verify.hpp"

#include <iostream>
#include <vector>
#include <fstream>
#include <cassert>
#include <cstdio>
#include <algorithm>
#include <iterator>
#include <ctime>

#include <boost/filesystem.hpp> 

#include <ext-libs/abc/abc_api.hpp>
#include <functions/maniac/mc_design.hpp>
#include <functions/maniac/mc_worst_case.hpp>

/*
  opts.add_options()
    ( "golden,g", value( &golden ), "Golden Design file (verilog)" )
    ( "approx,a", value( &approx ), "Approximated Design file (verilog)" )
    ( "golden_module,1", value( &golden_module ), "Golden Module to be verified" )
    ( "approx_module,2", value( &approx_module ), "Approx Module to be verified" )
    ( "port,p",   value( &port ),   "Output to be verified" )
    ( "accum_bitflip",  value( &accum_bitflip ),   "Accum bitflip [count (bit_flips()]" )
    ( "accum_error", value( &accum_error ),"Max accumulated error " )
    ( "error_cyclelimit,c", value_with_default( &error_cyclelimit ),
      "Number of clock cycles to check for accumulated error. [Default = 100]" )
    ( "bitflip_cyclelimit", value_with_default( &bitflip_cyclelimit ),
      "Number of clock cycles to check for accumulated bitflip. [Default = 100]" )
    ( "log,l",    value( &log_file ), "Log file [Default = cirkit_maniac.log]" )
    ( "debug,d",  value_with_default( &debug ),
      "Extra debug information [Default = 1 (true), Recommended to keep true]" )
    ( "clock",    value_with_default( &clock ), "Clock signal name [Default = clock]" )
    ( "reset",    value_with_default( &reset ), "Reset signal name [Default = rst]" )
    ( "oe",    value_with_default( &oe ),
      "Output-Enable signal, 1bit output signal  [Default = __NA__ (No output-enable signal)]" ) 
    ( "report",   value_with_default( &report_file ), "Output report [Default = maniac.rpt]" )
    ( "signed_outputs", value_with_default( &signed_outputs ),
      "If the outputs are signed numbers [Default = false]" )
    ( "worst_case_en", value_with_default( &worst_case_en ),"worst-case enable/disable " )
    ( "accum_error_en", value_with_default( &accum_error_en ),"accum-error enable/disable " )
    ( "max_bf_en", value_with_default( &max_bf_en ),"worst-bitflip enable/disable " )
    ( "accum_bitflip_en", value_with_default( &accum_bitflip_en ),"accum-bitflip enable/disable " )
    ( "avg_case_en", value_with_default( &avg_case_en ),"avg-case enable/disable " )
    ( "opt", value_with_default( &opt ),"heavy optimization procedures " )
    ( "bf_upper_bound", value_with_default( &bf_upper_bound ),"Upper bound for worst bitflip " )
    ( "bf_lower_bound", value_with_default( &bf_lower_bound ),"Lower bound for worst bitflip " )
    ;
*/

namespace maniac
{

void ManiacVerify::init_work_dirs() {
  std::srand ( unsigned  (std::time(0)) );  
  work = ".maniac_verify" +  boost::lexical_cast<std::string> (rand());
  worst_case_work = work + std::string("/worst_case"); 
  accum_case_work = work + std::string("/accum_case"); 
  max_bf_work     = work + std::string("/max_bf");	 
  accum_er_work   = work + std::string("/accum_er");	 
  avg_case_work   = work + std::string("/avg_case");     
}

//------------------------------------------------------------------------------
void ManiacVerify::verify() {
  auto start_time = curr_time();
  const auto print_cex = false;
  
  std::cout <<"[i] Maniac Circuit Verifier - started on " << curr_time();
  init_work_dirs();
  if (report_file != "NA") dump_report = true;
  axekit::create_work_dir( work ); // Create the work directory.
  
  // Yosys::yosys_setup(); // initialize yosys
  //if (debug) std::cout << "[i] " << Yosys::yosys_version_str << std::endl;

  const auto yosys_debug  = false;
  auto gold = mc_design (golden, golden_module, clock, reset, oe, yosys_debug);
  auto appx = mc_design (approx, approx_module, clock, reset, oe, yosys_debug);
  port_to_check = std::make_pair( port, gold.get_width (port) );

  // Do a first round of sanity checks.
  assert ( check_sanity (gold, appx) );
  
  std::ofstream file;
  if (dump_report) {
    // saving the result whenever available. anticipating long run-times/crashes.
    file.open (report_file);
    file << "Maniac Sequential Approximation Verifier Report " << std::endl;
    file << "Start Time  :: "  << start_time;
    file << "------------------------------------------------" << std::endl;
    print_program_options(file);
    file << "------------------------------------------------" << std::endl;
    file << "Results" << std::endl;
    file.close();
  }
  
  //abc::Abc_Start();

  // 1. Worst Case Type 2
  unsigned long wc = 0u;
  if (worst_case_en > 0) {
    if (0 != debug)
      std::cout << "----------------- Checking WORST CASE ----------------- :: " << curr_time();
    axekit::create_work_dir( worst_case_work ); // Create the work directory.
    const clock_t begin_time = std::clock();
    wc = mc_worst_case (gold, appx, port_to_check, signed_outputs,
			worst_case_work, debug, opt);
    wc_time = float ( std::clock() - begin_time ) / CLOCKS_PER_SEC;
    wc_res = {wc, wc_time};
  }
  
  if (dump_report) {
    file.open (report_file, std::ios::app);
    file << "Worst Case   :: " << wc << std::endl;
    file << "Time for WC  :: " << wc_time << std::endl;
    file.close();
  }
  
  // 2. Accumulated Worst Case Type 1
  int acc_cycles = 0;
  if (accum_error_en > 0) {
    if (0 != debug)
      std::cout << "----------------- Checking ACCUM ERROR ----------------- :: " << curr_time();
    axekit::create_work_dir( accum_case_work ); // Create the work directory.
    const auto verify_wc = cross_verify;
    const auto print_cex_wc = false;
    const clock_t begin_time = std::clock();
    acc_cycles = mc_accum_error_cycles ( gold, appx, port_to_check,
					 accum_error, error_cyclelimit,
					 signed_outputs, accum_case_work,
					 debug, verify_wc, print_cex_wc );
    // NOTE :: For the accumulated case, there is an extra cycle needed for accumulation.
    // Need to subtract this number.
    acc_cycles --;
    acc_time = float ( std::clock() - begin_time ) / CLOCKS_PER_SEC;
    accum_error_res = {acc_cycles, acc_time};
  }
  
  if (dump_report) {
    file.open (report_file, std::ios::app);
    file << "Cycles for Max Accumulated Error of " << accum_error << "  :: "
	 << acc_cycles << std::endl;
    file << "Time for ACC CASE  :: " << acc_time << std::endl;
    file.close();
  }
  
  // 3. Max Bit flips Type 4
  unsigned long bf = 0u;
  if (max_bf_en > 0) {
    if (0 != debug)
      std::cout << "----------------- Checking MAX BIT FLIPS ----------------- :: " << curr_time();
    const clock_t begin_time = std::clock();
    axekit::create_work_dir( max_bf_work ); // Create the work directory.
    bf = mc_max_bitflip (gold, appx, port_to_check, signed_outputs,
			 max_bf_work, debug, opt, bf_lower_bound, bf_upper_bound);
    bf_time = float ( std::clock() - begin_time ) / CLOCKS_PER_SEC;
    bf_res = {bf, bf_time};
  }

  if (dump_report) {
    file.open (report_file, std::ios::app);
    file << "Max Bit Flips         :: " << bf << std::endl;
    file << "Time for MAX BITFLIP  :: " << bf_time << std::endl;
    file.close();
  }
  
  // 4. Accumulated Error Rate Type 3
  int acc_cycles_bf = 0;
  if (accum_bitflip_en > 0) {
    if (0 != debug)
      std::cout << "----------------- Checking ACCUM ERROR RATE -------------- :: " << curr_time();
    axekit::create_work_dir( accum_er_work ); // Create the work directory.
    const auto verify_er = cross_verify;
    const auto print_cex_er = false;
    const clock_t begin_time = std::clock();
    acc_cycles_bf = mc_accum_bitflip_cycles ( gold, appx, port_to_check,
					      accum_bitflip, bitflip_cyclelimit,
					      signed_outputs, accum_er_work,
					      debug, verify_er, print_cex_er );
    // NOTE :: For the accumulated case, there is an extra cycle needed for accumulation.
    // Need to subtract this number.
    acc_cycles_bf --;
    acc_bf_time = float ( std::clock() - begin_time ) / CLOCKS_PER_SEC;
    accum_bf_res = {acc_cycles_bf, acc_bf_time};
  }
  
  if (dump_report) {
    file.open (report_file, std::ios::app);
    file << "Cycles for Accumulated Error Rate of " << accum_bitflip << "  :: "
	 << acc_cycles_bf << std::endl;
    file << "Time for ACCERR      :: " << acc_bf_time << std::endl;
    file.close();
  }
  auto finish_time = curr_time();
  //abc::Abc_Stop();
  if (dump_report) {
    file.open (report_file, std::ios::app);
    file << "Finish Time :: " << finish_time;
    file.close ();
  }

  if (0 == debug) return;
  
  std::cout << std::endl;
  if (worst_case_en > 0)  std::cout << "RESULT: SEQ WORST CASE = " << wc << std::endl;
  if (accum_error_en > 0)
    std::cout << "RESULT: CYCLES FOR MAX ACCUMULATED ERROR  " << accum_error
	      << " = " << acc_cycles << std::endl;
  if (max_bf_en > 0)   std::cout << "RESULT: SEQ MAX BIT FLIPS = " << bf << std::endl;
  if (accum_bitflip_en > 0)
    std::cout << "RESULT: CYCLES FOR ACCUMULATED BIT FLIPS " << accum_bitflip
	      << " = " << acc_cycles_bf << std::endl;
  

}


void ManiacVerify::print_program_options (std::ofstream &file) {
  assert (file.is_open());
  // TODO: get the correct method for this.
  file << "Program Options" << std::endl;
  file << "golden,g         :: " << golden << std::endl;
  file << "approx,a         :: " << approx << std::endl;
  file << "golden_module,1  :: " << golden_module << std::endl;
  file << "approx_module,2  :: " << approx_module << std::endl;
  file << "accum_bitflip   :: " << accum_bitflip << std::endl;
  file << "avg_error          :: " << avg_error << std::endl;
  file << "accum_error        :: " << accum_error << std::endl;
  file << "error_cyclelimit,c           :: " << error_cyclelimit << std::endl;
  file << "bitflip_cyclelimit  :: " << bitflip_cyclelimit << std::endl;
  file << "port,p   :: "     << port << std::endl;
  file << "clock    :: "   << clock << std::endl;
  file << "reset    :: "   << reset << std::endl;
  file << "oe       :: "   << oe << std::endl;
  file << "log,l    :: "   << log_file << std::endl;
  file << "debug,d  :: "   << debug << std::endl;
  file << "report   :: "   << report_file << std::endl;
  file << "csv      :: "   << csv_file << std::endl;
  file << "signed_outputs  :: " << signed_outputs << std::endl;
  file << "worst_case_en   :: " << worst_case_en << std::endl;
  file << "accum_error_en  :: " << accum_error_en << std::endl;
  file << "max_bf_en     :: " << max_bf_en << std::endl;
  file << "accum_bitflip_en     :: " << accum_bitflip_en << std::endl;
  file << "avg_case_en     :: " << avg_case_en << std::endl;
  file << "cross_verify    :: " << cross_verify << std::endl;
  //file << "avg_case_effort_level :: " << avg_case_effort_level << std::endl;
  //file << "cycles_for_avg_error  :: " << cycles_for_avg_error  << std::endl;
    
}


// TODO: use std::move 
bool ManiacVerify::check_sanity (const mc_design &golden, const mc_design &approx)
{
  auto g_inputs = golden.get_input_ports ();
  auto a_inputs = approx.get_input_ports ();
  auto g_outputs = golden.get_output_ports ();
  auto a_outputs = approx.get_output_ports ();

  //---// Try this later. TODO
  //---// http://stackoverflow.com/questions/9778238/move-two-vectors-together
  //---mc_ports g_ports, a_ports;
  //---g_ports.reserve ( g_inputs.size() + g_outputs.size() );
  //---a_ports.reserve ( a_inputs.size() + a_outputs.size() );
  //---// http://stackoverflow.com/questions/9778238/move-two-vectors-together

  if ( ( g_inputs.size() + g_outputs.size() ) !=  ( a_inputs.size() + a_outputs.size() ) ) 
  {
    std::cout << "[e] Number of ports for golden and approx must be the same " << std::endl;
    return false;
  }

  assert ( g_inputs.size() == a_inputs.size() );
  for (auto i=0u; i < g_inputs.size(); i++)
  {
    auto g = g_inputs[i];
    auto a = a_inputs[i];
    if ( g.first != a.first )
    {
      std::cout << "[e] Name or order mismatch for the input port :: "  ;
      std::cout << " golden port = " << g.first << " VS approx port = " << a.first << std::endl;
      return false;
    }
    if ( g.second != a.second )
    {
      std::cout << "[e] Width mismatch for the input port :: " << g.first  ;
      std::cout << " golden width = " << g.second << " VS approx width = " << a.second << std::endl;
      return false;
    }
  }
  
  assert ( g_outputs.size() == a_outputs.size() );
  for (auto i=0u; i < g_outputs.size(); i++)
  {
    auto g = g_outputs[i];
    auto a = a_outputs[i];
    if ( g.first != a.first )
    {
      std::cout << "[e] Name or order mismatch for the output port :: "  ;
      std::cout << " golden port = " << g.first << " VS approx port = " << a.first << std::endl;
      return false;
    }
    if ( g.second != a.second )
    {
      std::cout << "[e] Width mismatch for the output port :: " << g.first  ;
      std::cout << " golden width = " << g.second << " VS approx width = " << a.second << std::endl;
      return false;
    }
  }

  // Check the specified acc bitflip is less than or equal to the width of port_to_check.
  if (accum_bitflip > port_to_check.second )
  {
    std::cout << "[w] Specified accumulated bitflip (--accum_bitflip) is higher than "
	      << "the width of output port to verify (--port)." << std::endl
	      << "[w] accum_bitflip is reset to width of " << port_to_check.first
	      << " [" << port_to_check.second << "]" << std::endl;
    accum_bitflip = port_to_check.second;
      
  }

  if (avg_case_effort_level > 0u)
    std::cout << "[w] Average Case Effort is more than 0u. Going to take a long time." << std::endl;

  return true;
}


// Getting paranoid with unsigned vs bool conversion!
inline bool ManiacVerify::true_or_false (unsigned value) {
  if (value == 0u) return false;
  else return true;
}

inline void ManiacVerify::update_avg_case_clock_cycles () {
  // Assert not a combinational case.
  assert (cycles_for_avg_error > 0);
  return;

  // REST OF THIS LOGIC IS WRONG.
  
  if (avg_case_effort_level > 0u) // Warn in case of purposeful sabotage.
  { 
    std::cout << "[w] Average Case Effort level is " << avg_case_effort_level << std::endl
	      << "[w] This will take a LoooooooooooNG time to conclude."
	      << " Highly recommended to keep --avg_case_effort_level 0" << std::endl;
    return;
  }

  // Adjust to the nearest multiple of 2^n
  auto msb_pos = msb_pos_of_unsigned (cycles_for_avg_error);
  assert (msb_pos < 32); // Not going to support clk cycles more than 2^31
  auto cycles = ( 1u << (msb_pos + 1) ) ;
  if (cycles != cycles_for_avg_error) {
    cycles_for_avg_error = cycles;
    std::cout << "[w] In avg-case computations cycles_for_avg_error updated to the value :: "
  	      << cycles << std::endl;
  }
}
  
}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:


