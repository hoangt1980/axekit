// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
/**
 * @file mc_bmc.hpp
 *
 * @brief 
 *
 * @author Arun <arun@informatik.uni-bremen.de>
 */

#ifndef MC_BMC_HPP
#define MC_BMC_HPP

#include <ext-libs/abc/abc_api.hpp>
#include <string>
#include <iostream>
#include <boost/dynamic_bitset.hpp>
#include <boost/regex.hpp>
#include <fstream>
#include <utils/common_utils.hpp>
#include <functions/gia/gia_bmc.hpp>

namespace maniac
{
int mc_bmc( const std::string &blif_file, const std::string &out_cex_file,
	    const std::string &results_file, unsigned cycle_limit, bool debug,
	    bool print_cex);

}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
