// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : bdd_logical.cpp
// @brief  : Logical functions to operate on bdd_function_t datatype.
//
// TODO: define operators for all these
//------------------------------------------------------------------------------

#include "bdd_logical.hpp"

namespace axekit
{
using namespace cirkit;

bdd_function_t xor_bdd (const bdd_function_t &x, const bdd_function_t &y) {
  auto mgr = x.first; // todo: check if both managers are same.
  assert (x.second.size() == y.second.size() && "XOR for only BDD vectors with same size");
  std::vector <BDD> result;
  for (auto i=0u; i<x.second.size(); i++) {
    result.emplace_back ( x.second[i] ^ y.second[i] );
  }
  return {mgr, result};
  
}


bdd_function_t and_bdd (const bdd_function_t &x, const bdd_function_t &y) {
  auto mgr = x.first; // todo: check if both managers are same.
  assert (x.second.size() == y.second.size() && "AND for only BDD vectors with same size");
  std::vector <BDD> result;
  for (auto i=0u; i<x.second.size(); i++) {
    result.emplace_back ( x.second[i] * y.second[i] );
  }
  return {mgr, result};
  
}

bdd_function_t or_bdd (const bdd_function_t &x, const bdd_function_t &y) {
  auto mgr = x.first; // todo: check if both managers are same.
  assert (x.second.size() == y.second.size() && "OR for only BDD vectors with same size");
  std::vector <BDD> result;
  for (auto i=0u; i<x.second.size(); i++) {
    result.emplace_back ( x.second[i] + y.second[i] );
  }
  return {mgr, result};
  
}

bdd_function_t not_bdd (const bdd_function_t &bddf) {
  auto bs = bddf.second;
  std::vector<BDD> neg ( bs.size() );
  for (auto i=0u; i<bs.size(); i++)  {
    neg[i] = !bs[i];
  }
  return {bddf.first, neg};
}



}


// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
