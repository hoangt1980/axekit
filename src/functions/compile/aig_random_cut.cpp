// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : aig_random_cut.cpp
// @brief  : 
//
// TODO : rewrite entire algorithm properly.
//
//        Reads in a Network (Abc_Aig type not Gia), writes out blif.
//        sorts the path, do a first cut, and then return.
//        Purpose is only to do some trivial approximation.
//------------------------------------------------------------------------------

#include "aig_random_cut.hpp"
#include <algorithm>

namespace axekit
{

using namespace cirkit;

//------------------------------------------------------------------------------
using mpi = boost::multiprecision::int256_t;
using mpf = boost::multiprecision::cpp_dec_float_100;

namespace // Some of these should/may come from the compile command.
{ 

auto debug = 0;          // debug level for printing messages
auto max_cut_size = 3;   // constant for one run. To experiment with different cut-sizes.
std::string oblif;

float t_time_sort_paths = 0u, t_time_apply_cut = 0u, t_time_generate_cuts = 0u;

}

inline void ppp (int i) {
  std::cout  << "here:" << i << "\n";
}

Network aig_random_cut (Network ntk) {
  assert ( abc_ntk_inline::is_network_good(ntk) );
  // Note: anyway we lose the network name, port-name etc.
  // For now, no need to waste time on annotating these.
  abc_ntk_inline::set_network_name (ntk, "dummy");

  //------------------------------------------------------------
  auto appx_ntk = abc_ntk_inline::copy_network ( ntk ); 
  abc_ntk_inline::set_network_name ( appx_ntk, "appx" );
  appx_ntk = abc_ntk_inline::strash_network (appx_ntk); // Re-strash 
  assert ( abc_ntk_inline::is_topologically_sorted (appx_ntk) );

  auto begin_time = std::clock ();
  // Take some 10000 nodes from the design.
  const auto MAX_NODES = 10000;
  std::vector<Object> node_list;
  Object pObj;
  int ii;
  Abc_NtkForEachObj (appx_ntk, pObj, ii)
  {
    if ( !abc_ntk_inline::is_node_good (pObj) ) continue;
    if ( !abc_ntk_inline::is_node_internal (pObj) ) continue;
    node_list.emplace_back (pObj);
    if ( node_list.size() > (MAX_NODES - 3) ) break;
  }

  std::random_shuffle ( node_list.begin(), node_list.end() );
  assert (node_list.size() != 0);

  // 6. Do the cutting.
  for (auto &node : node_list) { // loop only to avoid miscellaneous crasehes.
    if ( !abc_ntk_inline::is_node_good(node) ) continue;
    if ( !abc_ntk_inline::is_node_internal(node) ) continue;
    abc_ntk_inline::replace_node1_with_node2 ( appx_ntk, node,
					       abc_ntk_inline::get_const1 (appx_ntk) );
    break; // after first cut, exit
  } // for (auto &node : node_list)

  auto time_apply_cut = get_elapsed_time ( begin_time );
  t_time_apply_cut += time_apply_cut; // total time
   
  if ( !abc_ntk_inline::check_network (appx_ntk) )
    std::cout << "[e] Error. Approximated Network integrity failed..  :(" << std::endl;
  return appx_ntk;
  
}

//------------------------------------------------------------------------------
  
// return the output appx-network name, and all the timings.
std::tuple <float, float, float>  from_aig_random_cut () {
  return std::make_tuple (
    0.0, t_time_apply_cut, 0.0
    );
}
  
//------------------------------------------------------------------------------
} // namespace axekit



// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
