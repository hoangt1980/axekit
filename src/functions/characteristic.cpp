// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : characteristic.cpp
// @brief  : Characteristic function (chi) of a BDD
//

//------------------------------------------------------------------------------

#include "characteristic.hpp"

namespace axekit
{
using namespace cirkit;

bdd_function_t characteristic_function (const bdd_function_t &bddf) {
  assert (false && "Use Mathias cirkit::compute_characteristic() Instead \n");
  std::vector <BDD> shifted_bdds = bddf.second;
  auto cudd = bddf.first;
  for (const auto&bdd: bddf.second) {
    shifted_bdds.emplace_back (bdd);
  }
  auto chi = bddf.first.bddOne();
  for (auto i=0u; i<shifted_bdds.size(); i++) {
    chi = chi * ( !( cudd.bddVar(i) ) ^ shifted_bdds[i] );
  }

  return {cudd, {chi}};
}

//------------------------------------------------------------------------------
} // namespace axekit





// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
