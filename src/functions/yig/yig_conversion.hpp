// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : yig_conversion.hpp
// @brief  : yig <-> gia, yig <-> ntk conversions
//
// 
//------------------------------------------------------------------------------
#pragma once
#ifndef YIG_CONVERSION_HPP
#define YIG_CONVERSION_HPP

#include <cassert>
#include <iostream>
#include <fstream>
#include <algorithm>

#include <ext-libs/abc/abc_api.hpp>
#include <types/yig/yig.hpp> 

namespace axekit {

Yig::YigGraph_t gia_to_yig ( abc::Gia_Man_t *orig, const unsigned num_nodes );

}

#endif

//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
