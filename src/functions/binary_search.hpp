// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : binary_search.hpp
// @brief  : All binary search routines with SAT
//
// 
//------------------------------------------------------------------------------
#pragma once
#ifndef BINARY_SEARCH_HPP
#define BINARY_SEARCH_HPP

#include <cassert>
#include <iostream>
#include <algorithm>

#include <ext-libs/abc/abc_api.hpp>
#include <types/Path.hpp>
#include <types/AbcTypes.hpp>
#include <types/AppxSynthesisTypes.hpp>

#include <utils/abc_utils.hpp>
#include <utils/common_utils.hpp>

#include <functions/appx_miter.hpp>

namespace axekit {

unsigned binsearch_max_bit_flip ( const std::string &golden_verilog, const Design &approx_design, 
				  Network approx_network, const Port &port_to_check, 
				  const std::string &work, const unsigned &debug );


}

#endif

//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
