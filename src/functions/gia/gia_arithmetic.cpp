// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : gia_arithmetic.cpp
// @brief  : Arithemtic functions using Gia
//------------------------------------------------------------------------------


#include "gia_arithmetic.hpp"
#include <cstdio>
#include <proof/cec/cec.h>

namespace abc
{
void Wlc_BlastSubtract( Gia_Man_t * pNew, int * pAdd0, int * pAdd1, int nBits ); // result is in pAdd0
int Wlc_BlastLessSigned( Gia_Man_t * pNew, int * pArg0, int * pArg1, int nBits );
int Wlc_BlastLess( Gia_Man_t * pNew, int * pArg0, int * pArg1, int nBits );
}



namespace axekit
{

using namespace cirkit;

// po1 and po2 are vectors of literals of the original outputs
abc::Gia_Man_t * Gia_ManDupAppendNewWithoutPOs ( abc::Gia_Man_t * pOne, abc::Gia_Man_t * pTwo,
						 std::vector<int>& po1, std::vector<int>& po2 ) {
  abc::Gia_Obj_t * pObj;
  int i;
  abc::Gia_Man_t *pTemp;
  auto pNew = abc::Gia_ManStart( abc::Gia_ManObjNum( pOne ) + abc::Gia_ManObjNum( pTwo ) );
  pNew->pName = abc::Abc_UtilStrsav( pOne->pName );
  pNew->pSpec = abc::Abc_UtilStrsav( pOne->pSpec );
  abc::Gia_ManHashAlloc( pNew );
  abc::Gia_ManConst0( pOne )->Value = 0;
  Gia_ManForEachObj1( pOne, pObj, i )
  {
    if ( abc::Gia_ObjIsAnd( pObj ) )
    {
      pObj->Value = abc::Gia_ManHashAnd( pNew, abc::Gia_ObjFanin0Copy( pObj ), abc::Gia_ObjFanin1Copy( pObj ) );
    }
    else if ( abc::Gia_ObjIsPi( pOne, pObj ) )
    {
      pObj->Value = abc::Gia_ManAppendCi( pNew );
    }
  }
  abc::Gia_ManConst0( pTwo )->Value = 0;
  Gia_ManForEachObj1( pTwo, pObj, i )
  {
    if ( abc::Gia_ObjIsAnd( pObj ) )
    {
      pObj->Value = abc::Gia_ManHashAnd( pNew, abc::Gia_ObjFanin0Copy( pObj ), abc::Gia_ObjFanin1Copy( pObj ) );
    }
    else if ( abc::Gia_ObjIsPi( pTwo, pObj ) )
    {
      pObj->Value = abc::Gia_ManPi( pOne, abc::Gia_ObjCioId( pObj ) )->Value;
    }
  }

  po1.clear();
  po2.clear();

  Gia_ManForEachPo( pOne, pObj, i )
  {
    po1.push_back( Gia_ObjFanin0Copy( pObj ) );
  }
  Gia_ManForEachPo( pTwo, pObj, i )
  {
    po2.push_back( Gia_ObjFanin0Copy( pObj ) );
  }
  abc::Gia_ManSetRegNum( pNew, 0 );


  //abc::Gia_ManHashStop ( pNew );
  //pNew = abc::Gia_ManCleanup ( pTemp = pNew );
  //abc::Gia_ManStop ( pTemp );
  //
  //pNew = abc::Gia_ManDupNormalize ( pTemp = pNew, 0 );
  //abc::Gia_ManStop( pTemp );

  return pNew;
}




abc::Gia_Man_t * subtract_gia ( abc::Gia_Man_t *f, abc::Gia_Man_t *fhat ) {
  assert ( abc::Gia_ManPiNum (f) == abc::Gia_ManPiNum (fhat) );
  assert ( abc::Gia_ManPoNum (f) == abc::Gia_ManPoNum (fhat) );

  assert ( f->nRegs == 0 && "Need a clarity if this routine takes care of registers or not."); 
  
  auto num_bits = abc::Gia_ManPoNum (f);
  std::vector <int> po1, po2;
  // append f to fhat, and share PIs
  auto diff = Gia_ManDupAppendNewWithoutPOs ( f, fhat, po1, po2 );
  assert( abc::Gia_ManCiNum( diff ) == abc::Gia_ManPiNum (f) );
  assert( abc::Gia_ManCoNum( diff ) == 0 );
  assert( po1.size() == num_bits );
  assert( po2.size() == num_bits );

  /* create comparator */
  int * a = &po1[0];
  int * b = &po2[0];

  // Note: these numbers are NOT signed. Sign bit comes only after subtraction
  // which is taken care by the less and mux scheme.
  //const auto less = abc::Wlc_BlastLessSigned( diff, a, b, num_bits );
  const auto less = abc::Wlc_BlastLess( diff, a, b, num_bits );

  std::vector<int> m1( num_bits ), m2( num_bits );
  for ( auto i = 0u; i < num_bits; ++i )
  {
    m1[i] = abc::Gia_ManHashMux( diff, less, po2[i], po1[i] ); /* returns the larger number */
    m2[i] = abc::Gia_ManHashMux( diff, less, po1[i], po2[i] ); /* returns the smaller number */
  }

  /* create subtractor */
  abc::Wlc_BlastSubtract( diff, &m1[0], &m2[0], num_bits ); /* stores result in m1 */

  for ( auto l : m1 )
  {
    abc::Gia_ManAppendCo( diff, l );
  }
  abc::Gia_ManHashStop (diff);
  return diff;
  
}


boost::multiprecision::uint256_t get_max_value ( abc::Gia_Man_t *gia ) {
  auto bs = solve_max_lexsat (gia);
  return to_multiprecision<boost::multiprecision::uint256_t>( bs );
}

//---boost::multiprecision::uint256_t get_weighted_sum ( const abc::Gia_Man_t *bddf ) {
//---}
//---

// returns a vector {zerolit_0, zerolit_1,...zerolit_num, gia_1, gia_0, ...}
inline std::vector<int> create_gia_vector (abc::Gia_Man_t *gia, int lit, unsigned prepend_width) {
  std::vector<int> result (prepend_width + 1);
  std::fill ( result.begin(), result.end(), 0 ); // 0 is the constant-0 literal in ABC.
  result[0] = lit;
  return result;
}

// input = pOne_output[n-0]
// output = pOne_output[0] + pOne_output[1] + pOne_output[2] + .. + pOne_output[n]
abc::Gia_Man_t * add_individual_outputs (abc::Gia_Man_t *pOne) {
  
  abc::Gia_Obj_t * pObj;
  int i;
  auto pNew = abc::Gia_ManStart ( abc::Gia_ManObjNum( pOne ) );
  char name[] = "sum_of_outputs";
  pNew->pName = abc::Abc_UtilStrsav ( name );
  pNew->pSpec = abc::Abc_UtilStrsav ( pOne->pSpec );
  abc::Gia_ManHashAlloc( pNew );

  // Copy all the objects from pOne
  abc::Gia_ManConst0( pOne )->Value = 0;
  Gia_ManForEachObj1( pOne, pObj, i )
  {
    if ( abc::Gia_ObjIsAnd( pObj ) )  {
      pObj->Value = abc::Gia_ManHashAnd( pNew, abc::Gia_ObjFanin0Copy( pObj ), abc::Gia_ObjFanin1Copy( pObj ) );
    }
    else if ( abc::Gia_ObjIsPi( pOne, pObj ) )  {
      pObj->Value = abc::Gia_ManAppendCi( pNew );
    }
  }

  if ( 1 == abc::Gia_ManPoNum(pOne) ) { // case of only one output.
    abc::Gia_ManAppendCo (pNew, abc::Gia_ObjFanin0Copy ( abc::Gia_ManCo (pOne, 0) ) );
    return pNew;
  }

  std::pair < std::vector<int>, int > result;
  int cin = 0; // constant 0 literal
  std::vector <int> ygia = {0}; // constant 0 literal.
  for (auto i=0u; i < abc::Gia_ManPoNum(pOne); i++) {
    auto lit = abc::Gia_ObjFanin0Copy ( abc::Gia_ManCo (pOne, i) );
    auto x = create_gia_vector ( pNew, lit, ygia.size() - 1 ); // prepend so many zeros as ygia
    result = full_adder (pNew, x, ygia, cin);
    ygia = result.first;
    ygia.emplace_back (result.second);
  }
  
  for (auto i=0; i < result.first.size(); i++ ) {
    abc::Gia_ManAppendCo ( pNew, result.first[i] );
  }
  abc::Gia_ManAppendCo ( pNew, result.second );
  return pNew;
}


// Results are returned, but not registered as outputs.
std::pair < std::vector<int>, int > full_adder ( abc::Gia_Man_t *gia, const std::vector<int> &x,
						 const std::vector<int> &y, int cin ) {

  assert ( x.size() == y.size() );
  assert ( !x.empty() );
  std::vector<int> sum;
  int cx = cin;

  for (auto i=0u; i < x.size(); i++) {
    sum.emplace_back ( abc::Gia_ManHashXor ( gia, abc::Gia_ManHashXor (gia, x[i], y[i]), cx ) );
    auto p = abc::Gia_ManHashAnd ( gia, x[i], y[i] );
    auto q = abc::Gia_ManHashAnd ( gia, x[i], cx );
    auto r = abc::Gia_ManHashAnd ( gia, y[i], cx );
    cx = abc::Gia_ManHashOr ( gia, abc::Gia_ManHashOr (gia, p, q), r );  
  }
  
  return {sum, cx};
}


//---// register_gia will register the output and return a new Gia.
//---abc::Gia_Man_t* register_gia ( abc::Gia_Man_t *orig_gia ) {
//---  auto gia = abc::Gia_ManDup ( orig_gia );
//---  pNew = abc::Gia_ManStart ( abc::Gia_ManObjNum (gia) );
//---  pNew->pName = abc::Abc_UtilStrsav( "accumulater" );
//---
//---
//---  // Abc_NtkMakeSeq
//---  // Abc_NtkLatchPipe( pNtk, nLatches ); // append latches to the input
//---  // pNtkRes = Abc_NtkAigToSeq( pNtk );  // some unknown command
//---}
//---


}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
