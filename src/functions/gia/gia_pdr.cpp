// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : gia_pdr.cpp
// @brief  : Property directed reachability.
//
//------------------------------------------------------------------------------

#include "gia_pdr.hpp"

#include <proof/pdr/pdr.h>

namespace axekit
{

namespace {
// redirect printf() output to a string.
// http://stackoverflow.com/questions/19485536/
//        redirect-output-of-an-function-printing-to-console-to-string

const auto BUFFSIZE = 4096 * 2;
FILE *org_stdout, *org_stderr;
FILE *tmp_stdout, *tmp_stderr;
char buffer_stdout[BUFFSIZE];
char buffer_stderr[BUFFSIZE];

inline void close_stdout() {
  org_stdout = stdout;
  tmp_stdout = fmemopen (buffer_stdout, BUFFSIZE, "w");
  stdout = tmp_stdout;
}
inline void close_stderr() {
  org_stderr = stderr; 
  tmp_stderr = fmemopen (buffer_stderr, BUFFSIZE, "w");
  stderr = tmp_stderr;
}

inline void restore_stdout () {
  std::fclose (tmp_stdout);
  stdout = org_stdout;
}

inline void restore_stderr () {
  std::fclose (tmp_stderr);
  stderr = org_stderr;
}

}


// This PDR will return if the pNtk property holds or not.
bool pdr ( abc::Abc_Ntk_t *pNtk ) {
  assert ( abc::Abc_NtkLatchNum (pNtk) != 0 ); // Must be sequential.
  abc::Aig_Man_t *pMan;
  pMan = abc::Abc_NtkToDar ( pNtk, 0, 1 );
  if (pMan == nullptr) {
    std::cout << "[e] PDR error. Conversion failed.\n";
    return false;
  }
  // make pPars.
  abc::Pdr_Par_t Pars; auto *pPars = &Pars;
  abc::Pdr_ManSetDefaultParams (pPars);

  bool status = false;
  close_stdout(); close_stderr();
  auto ret = abc::Pdr_ManSolve ( pMan, pPars );
  restore_stdout(); restore_stderr();

  //pPars->nDropOuts = abc::Saig_ManPoNum(pMan) - pPars->nProveOuts - pPars->nFailOuts;
  //std::cout << "PDR Return = " << ret << "\n";
  if ( 1 == ret ) status = false;
  else if ( 0 == ret ) status = true;
  else if ( -1 == ret) {
    status = false;
    std::cout << "[w] PDR: property undecided\n";
  }
  else assert (false); 
 
  ABC_FREE( pNtk->pSeqModel );
  pNtk->pSeqModel = pMan->pSeqModel;
  pMan->pSeqModel = nullptr;
  if ( pNtk->vSeqModelVec )
    Vec_PtrFreeFree( pNtk->vSeqModelVec );
  pNtk->vSeqModelVec = pMan->vSeqModelVec;
  pMan->vSeqModelVec = nullptr;
  abc::Aig_ManStop( pMan );
  return status;
  
}


bool pdr ( abc::Gia_Man_t *gia ) {
  assert ( gia->nRegs > 0 ); // must be sequential.
  // Until we find a proper way to find gia to ntk conversion, take this route.
  // See aig_to_ntk.cpp for other attempts.
  char ntk_file[] = "/tmp/pdr.aig";
  abc::Gia_AigerWrite ( gia, ntk_file, 1, 0 );    
  auto ntk = abc::Io_ReadAiger ( ntk_file, 1 );
  ntk = abc::Abc_NtkToLogic (ntk); // Convert to Logic form.
  ntk = abc::Abc_NtkStrash (ntk, 1, 1, 0); // Strash
  abc::Abc_AigCleanup((abc::Abc_Aig_t *)ntk->pManFunc); // Cleanup
  ntk = abc::Abc_NtkStrash (ntk, 1, 1, 0); // Strash again
  ntk->pName = abc::Abc_UtilStrsav ( gia->pName );
  assert ( abc::Abc_NtkLatchNum (ntk) != 0 );
  
  auto status = pdr (ntk);
  abc::Abc_NtkDelete (ntk); // cleanup
  return status;
  
}


}


// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
