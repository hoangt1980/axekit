// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : cudd_cirkit.cpp
// @brief  : Cirkit routines ported to axekit
//           
//           
//------------------------------------------------------------------------------

#include "cudd_cirkit.hpp"

#include <functional>
#include <iostream>
#include <sstream>
#include <cassert>

#include <boost/format.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/range/algorithm_ext/push_back.hpp>
#include <boost/range/counting_range.hpp>

#include <cuddInt.h>
#include <cirkit/bitset_utils.hpp>

namespace axekit
{
using namespace cirkit;

boost::multiprecision::uint256_t count_solutions ( const bdd_function_t& f) {

  assert ( f.second.size() == 1  && "count_solutions works only in single BDD (given vector<BDD>)");
  assert ( !f.second.empty() && "Empty BDD given to count_solutions");
  
  auto h = f.second[0];
  std::stringstream s;
  s.precision(0);
  s << std::fixed << h.CountMinterm( f.first.ReadSize() );
  return boost::multiprecision::uint256_t( s.str() );  
}



boost::multiprecision::uint256_t count_solutions ( DdNode* f, const Cudd &mgr, unsigned nvars) {
  const auto ddMgr = mgr.getManager();
  std::stringstream s;
  s.precision(0);
  s << std::fixed << Cudd_CountMinterm ( ddMgr, f, nvars );
  return boost::multiprecision::uint256_t( s.str() );  
}



}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
