#!/usr/bin/env bash

if [ -z $1 ]; then echo "Usage: ./new_command.sh <command-name>"; exit; fi

if [ -e ${1}.hpp ]; then echo "[e] ${1}.hpp exists!"; exit; fi
if [ -e ${1}.cpp ]; then echo "[e] ${1}.cpp exists!"; exit; fi

cp -f template.hpp ${1}.hpp
cp -f template.cpp ${1}.cpp

sed -i "s/template/${1}/g" ${1}.hpp
sed -i "s/template/${1}/g" ${1}.cpp

upper=`echo ${1} | tr '[:lower:]' '[:upper:]' `
sed -i "s/TEMPLATE/${upper}/g" ${1}.hpp
sed -i "s/TEMPLATE/${upper}/g" ${1}.cpp

