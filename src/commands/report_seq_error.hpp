// Copyright (C) AGRA - University of Bremen
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : report_seq_error.hpp
// @brief  : Report the various error metrics. (error-rate, worst-case, bit-flip)
//
//------------------------------------------------------------------------------
#pragma once

#ifndef REPORT_SEQ_ERROR_HPP
#define REPORT_SEQ_ERROR_HPP

#include <commands/axekit_stores.hpp>
#include <boost/multiprecision/cpp_int.hpp>
#include <boost/multiprecision/cpp_dec_float.hpp>
#include <cirkit/cudd_cirkit.hpp>
#include <functions/bdd/bdd_error_metrics.hpp>
#include <functions/gia/gia_error_metrics.hpp>

namespace alice {

class report_seq_error_command : public command {
public:
  report_seq_error_command (const environment::ptr &env) : command (env, "Report different approximation error metrics")
  {
    //add_positional_option ("report");
  opts.add_options()
    ( "golden,g", po::value( &golden ), "Golden Design file (verilog)" )
    ( "approx,a", po::value( &approx ), "Approximated Design file (verilog)" )
    ( "golden_module,1", po::value( &golden_module ), "Golden Module to be verified" )
    ( "approx_module,2", po::value( &approx_module ), "Approx Module to be verified" )
    ( "port,p",   po::value( &port ),   "Output port to be verified" )
    ( "accum_bitflip",  po::value( &accum_bitflip ),   "Accum bitflip [count (bit_flips()]" )
    ( "accum_error", po::value( &accum_error ),"Max accumulated error " )
    ( "error_cyclelimit", po::value( &error_cyclelimit )->default_value (error_cyclelimit),
      "Number of clock cycles to check for accumulated error. [Default = 100]" )
    ( "bitflip_cyclelimit", po::value( &bitflip_cyclelimit )->default_value (bitflip_cyclelimit),
      "Number of clock cycles to check for accumulated bitflip. [Default = 100]" )
    ( "log,l",    po::value( &log_file )->default_value (log_file), "Log file" )
    ( "debug,d",  po::value( &debug )->default_value (debug), "Extra debug information" )
    ( "clock",    po::value( &clock )->default_value (clock), "Clock signal name" )
    ( "reset",    po::value( &reset )->default_value (reset), "Reset signal name" )
    ( "oe",       po::value( &oe )->default_value (oe), "Output-Enable signal name (1-bit)" ) 
    ( "report",   po::value( &report_file )->default_value (report_file), "Separate Maniac report file" )
    ( "signed_outputs", "If the outputs are signed numbers [Default = No]" )
    ( "no_wc",          "worst-case computations disable [Default = enable]" )
    ( "no_accum_error", "accum-error disable [Default = enable]" )
    ( "no_max_bf",      "max-bitflip disable [Default = enable]" )
    ( "no_accum_bf",    "accum-bitflip disable [Default = enable]" )
    ( "opt",            "extra optimization procedures [Default = disable]" )
    ( "bf_upper_bound", po::value( &bf_upper_bound )->default_value (bf_upper_bound),"Upper bound for max bitflip " )
    ( "bf_lower_bound", po::value( &bf_lower_bound )->default_value (bf_lower_bound),"Lower bound for max bitflip \n\nBug: One dummy read_verilog is needed to initialize properly." )
    ;


  }


  
protected:
  // these 3 funtions, validity_rules(), execute() and log() defines the behavior of the command.
  // keep this as it is, in every command header. log() and validity_rules() are optional.
  
  /* rules to check before execution:
   *
   * rules_t is a vector of pairs, each pair is a predicate and a string:
   * predicates are checked in order, if they are false, the command is not executed and the string is
   * outputted as error.
   */
  rules_t validity_rules() const;

  // the main execution part of the command.
  bool execute(); 

  // Log for tool execution in JSON format
  log_opt_t log() const;

private:
  std::string golden;
  std::string approx;
  std::string golden_module;
  std::string approx_module;
  std::string port;
  std::string clock = "clock";
  std::string reset = "rst";
  std::string oe = "__NA__";
  unsigned error_cyclelimit = 100u;
  unsigned bitflip_cyclelimit = 100u;
  unsigned accum_bitflip = 0u;
  unsigned accum_error = 0u;
  unsigned avg_error = 0u;
  std::string log_file = "axc_maniac_seq.log";
  std::string report_file = "NA";
  std::string csv_file = "axc_maniac_seq.csv";
  unsigned debug = 0; // Default in debug mode.
  unsigned signed_outputs = 0u; // To indidcate if inputs are signed or not.

  unsigned worst_case_en = 1u; // If worst-case computations are enabled.
  unsigned accum_error_en = 1u; // If acc error computations are enabled.
  unsigned max_bf_en = 1u; // If worst-bitflip computations are enabled.
  unsigned accum_bitflip_en = 1u; // If accum-bitflip computations are enabled.
  unsigned avg_case_en = 1u; // If avg-case computations are enabled.
  bool cross_verify = false;  //  cross-verify the accum computations results.

  // avg_error = accumulated_error / cycles_for_avg_error;
  // if its 0u, then division will be rounded to nxt shifting.
  // if its 1u, then division will be with a constant
  // if its 2u, then generic division will 
  unsigned avg_case_effort_level = 0u;
  unsigned cycles_for_avg_error = 256u;

  unsigned opt = 0u;
  unsigned bf_upper_bound = 0u;
  unsigned bf_lower_bound = 0u;


  // Final results
  unsigned long worst_case_error = 0u;
  float time_worst_case_error = 0.0, time_max_bitflip_error = 0.0;
  float time_cycles_for_accumulated_error = 0.0, time_cycles_for_accumulated_bitflip = 0.0;
  unsigned long max_bitflip_error = 0u;
  int cycles_for_accumulated_error = 0;
  int cycles_for_accumulated_bitflip = 0;
  
};

}

//------------------------------------------------------------------------------

#endif
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
