// Copyright (C) AGRA - University of Bremen
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : logical.hpp
// @brief  : AND, OR, XOR and NOT of two functions
//
//------------------------------------------------------------------------------
#pragma once

#ifndef LOGICAL_HPP
#define LOGICAL_HPP

#include <commands/axekit_stores.hpp>
#include <functions/bdd/bdd_logical.hpp>

namespace alice {

class logical_command : public command {
public:
  logical_command (const environment::ptr &env) : command (env, "Compute the logical functions")
  {
    opts.add_options()
      ( "id1", po::value( &id1 )->default_value( id1 ), "store id of first circuit" )
      ( "id2", po::value( &id2 )->default_value( id2 ), "store id of second circuit" )
      ( "store,s", po::value( &store )->default_value( store ), "Which store to use? (BDD=0 / Gia=1)" )
      ( "new,n" ,                                "Create a new store entry" )

      ( "and" ,                                  "id1 & id2  :  logical AND" )
      ( "or" ,                                   "id1 | id2  :  logical OR" )
      ( "xor" ,                                  "id1 ^ id2  :  logical XOR" )
      ( "not" ,                                  "!id1       :  negate id1 (id2 is ignored)" )

      ;
  }

protected:
  rules_t validity_rules() const;

  // the main execution part of the command.
  bool execute(); 

  // Log for tool execution in JSON format
  log_opt_t log() const;

private:
  unsigned id1 = 0;
  unsigned id2 = 1;
  unsigned store = 0;
};

}

//------------------------------------------------------------------------------

#endif
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
