// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : template.cpp
// @brief  : 
//           
//           
//------------------------------------------------------------------------------

#include "template.hpp"

namespace alice
{

namespace {
// Private functions.
}

template_command::rules_t template_command::validity_rules() const {
  return {
    // rules are pairs, first is a predicate and, next is a string when predicate is false.
    // RULE FORMAT::    { [this]() {} , ""  }
    // rule-1. file should have verilog extension.
    { [this]() { return false; } , "Not a VERILOG?"  }
  };
}

bool template_command::execute() {

  return true;
}

template_command::log_opt_t template_command::log() const  {
  return log_opt_t (
    {
      {"file", file},
      }
    );
}


} // namespace alice
//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
