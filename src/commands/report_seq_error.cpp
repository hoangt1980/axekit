// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : report_seq_error.cpp
// @brief  : report sequential error-metrics.
//           
//           
//------------------------------------------------------------------------------

#include "report_seq_error.hpp"
#include <fstream>
#include <utils/common_utils.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/filesystem.hpp>
#include <functions/maniac/mc_verify.hpp>

namespace alice
{

// predicate for validity-rule 1.
bool check_extension (const std::string &golden, const std::string &approx) {
  auto ext1 = axekit::get_extension_of_file (golden);
  auto ext2 = axekit::get_extension_of_file (approx);
  if ( !((ext1 == ".v") || (ext1 == ".V")) ) return false;
  if ( !((ext2 == ".v") || (ext2 == ".V")) ) return false;
  return true;
}

// predicate for validity-rule 2.
bool do_files_exist (const std::string &golden, const std::string &approx)  {
  boost::filesystem::path p1(golden);
  if ( !boost::filesystem::exists (p1) ) return false;
  boost::filesystem::path p2(approx);
  if ( !boost::filesystem::exists (p2) ) return false;
  return true;
}


report_seq_error_command::rules_t report_seq_error_command::validity_rules()  const {	
  return {
    { [this]() {
	return do_files_exist (golden, approx);
      }, "Specified golden/approx file does not exist!" },

    { [this]() {
	return check_extension (golden, approx);
      }, "Only verilog supported. Golden/approx files are not valid verilog!" }
      
  };
}


bool report_seq_error_command::execute() {
  auto mc_verify = maniac::ManiacVerify();
  if ( is_set("signed_outputs") ) signed_outputs = 1;
  if ( is_set("no_wc") ) worst_case_en = 0;
  if ( is_set("no_accum_error") ) accum_error_en = 0;
  if ( is_set("no_max_bf") ) max_bf_en = 0;
  if ( is_set("no_accum_bf") ) accum_bitflip_en = 0;
  if ( is_set("opt") ) opt = 1;
  
  mc_verify.golden = golden;
  mc_verify.approx = approx;
  mc_verify.golden_module = golden_module;
  mc_verify.approx_module = approx_module;
  mc_verify.port = port;
  mc_verify.clock = clock;
  mc_verify.reset = reset;
  mc_verify.oe = oe;
  mc_verify.error_cyclelimit = error_cyclelimit;
  mc_verify.bitflip_cyclelimit = bitflip_cyclelimit;
  mc_verify.accum_bitflip = accum_bitflip;
  mc_verify.accum_error = accum_error;
  mc_verify.avg_error = avg_error;
  mc_verify.log_file = log_file;
  mc_verify.report_file = report_file;
  mc_verify.csv_file = csv_file;
  mc_verify.debug = debug;
  mc_verify.signed_outputs = signed_outputs;
  mc_verify.worst_case_en = worst_case_en;
  mc_verify.accum_error_en = accum_error_en;
  mc_verify.max_bf_en = max_bf_en;
  mc_verify.accum_bitflip_en = accum_bitflip_en;
  mc_verify.avg_case_en = avg_case_en;
  mc_verify.cross_verify = cross_verify;
  mc_verify.avg_case_effort_level = avg_case_effort_level;
  mc_verify.cycles_for_avg_error = cycles_for_avg_error;
  mc_verify.opt = opt;
  mc_verify.bf_upper_bound = bf_upper_bound;
  mc_verify.bf_lower_bound = bf_lower_bound;                                      

  mc_verify.verify();


  if ( !is_set("no_wc") ) {
    std::cout << "worst-case error : " << mc_verify.get_worst_case().first << "\n";
  }
  if ( !is_set("no_accum_error") ) {
    std::cout << "cycles for accumulated error "<< accum_error << " : "
	      << mc_verify.get_acc_error().first << "\n";
  }
  if ( !is_set("no_max_bf") ) {
    std::cout << "max bitfllip error : " << mc_verify.get_max_bitflip().first << "\n";
  }
  if ( !is_set("no_accum_bf") ) {
    std::cout << "cycles for accumulated bitflips "<< accum_bitflip << " : "
	      << mc_verify.get_acc_bitflip().first << "\n";
  }
 
  worst_case_error =          ( mc_verify.get_worst_case().first ) ;
  time_worst_case_error =     ( mc_verify.get_worst_case().second ) ;
  max_bitflip_error =         ( mc_verify.get_max_bitflip().first ) ;
  time_max_bitflip_error =    ( mc_verify.get_max_bitflip().second ) ;
  cycles_for_accumulated_error =         ( mc_verify.get_acc_error().first ) ;
  time_cycles_for_accumulated_error =    ( mc_verify.get_acc_error().second ) ;
  cycles_for_accumulated_bitflip =       ( mc_verify.get_acc_bitflip().first ) ;
  time_cycles_for_accumulated_bitflip =  ( mc_verify.get_acc_bitflip().second );


  
  return true;
}



report_seq_error_command::log_opt_t report_seq_error_command::log() const  {
  return log_opt_t (
    {
      {"golden",	    golden },
      {"approx",	    approx },
      {"golden_module",     golden_module },
      {"approx_module",     approx_module },
      {"port",	            port },
      {"clock",	            clock },
      {"reset",	            reset },
      {"oe",	            oe },
      {"log_file",	    log_file },
      {"report_file",       report_file },
      {"csv_file",          csv_file        },
      {"cross_verify",            boost::lexical_cast<std::string> ( cross_verify ) },
      {"debug",		          boost::lexical_cast<std::string> ( debug ) },
      {"signed_outputs",          boost::lexical_cast<std::string> ( signed_outputs ) },
      {"worst_case_en",	          boost::lexical_cast<std::string> ( worst_case_en ) },
      {"accum_error_en",          boost::lexical_cast<std::string> ( accum_error_en ) },
      {"max_bf_en",	          boost::lexical_cast<std::string> ( max_bf_en ) },
      {"accum_bitflip_en",        boost::lexical_cast<std::string> ( accum_bitflip_en ) },
      {"avg_case_en",	          boost::lexical_cast<std::string> ( avg_case_en ) },
      {"avg_case_effort_level",   boost::lexical_cast<std::string> ( avg_case_effort_level ) },
      {"cycles_for_avg_error",    boost::lexical_cast<std::string> ( cycles_for_avg_error ) },
      {"opt",		          boost::lexical_cast<std::string> ( opt ) },
      {"bf_upper_bound",          boost::lexical_cast<std::string> ( bf_upper_bound ) },
      {"bf_lower_bound",          boost::lexical_cast<std::string> ( bf_lower_bound ) },
      {"input_error_cyclelimit",        boost::lexical_cast<std::string> ( error_cyclelimit ) },
      {"input_bitflip_cyclelimit",      boost::lexical_cast<std::string> ( bitflip_cyclelimit ) },
      {"input_accumulated_bitflip",	boost::lexical_cast<std::string> ( accum_bitflip ) },
      {"input_accumulated_error",	boost::lexical_cast<std::string> ( accum_error ) },
      {"avg_error",               boost::lexical_cast<std::string> ( avg_error ) },
      {"worst_case_error",        boost::lexical_cast<std::string> ( worst_case_error ) },
      {"time_worst_case_error",   boost::lexical_cast<std::string> ( time_worst_case_error ) },
      {"max_bitflip_error",       boost::lexical_cast<std::string> ( max_bitflip_error ) },
      {"time_max_bitflip_error",  boost::lexical_cast<std::string> ( time_max_bitflip_error ) },
      {"cycles_for_accumulated_error",       boost::lexical_cast<std::string> ( cycles_for_accumulated_error ) },
      {"time_cycles_for_accumulated_error",  boost::lexical_cast<std::string> ( time_cycles_for_accumulated_error) },
      {"cycles_for_accumulated_bitflip",     boost::lexical_cast<std::string> ( cycles_for_accumulated_bitflip ) },
      {"time_cycles_for_accumulated_bitflip",boost::lexical_cast<std::string> ( time_cycles_for_accumulated_bitflip ) }
      
    }
    
    );
}



//---void report_seq_error_command::write_report() {
//---  std::ofstream rpt (rpt_file);
//---  rpt << "AxC approximation error report  : " << axekit::curr_time() << "\n";
//---  rpt << "Report file                     : " << rpt_file   << "\n";
//---  rpt << "Error Count                     : " << error_rate << "\n";
//---  rpt << "Error Rate                      : " << error_rate << "\n";
//---  rpt << "Worst-Case Error                : " << wc_error  << "\n";
//---  rpt << "Worst-Case Error (Chi Method)   : " << wc_error_chi  << "\n";
//---  rpt << "Average-Case Error              : " << avg_error  << "\n";
//---  rpt << "Bit-Flip Error                  : " << bf_error  << "\n";
//---  rpt << "Bit-Flip Error (Chi Method)     : " << bf_error_chi  << "\n";
//---  rpt << "Time for error-rate             : " << er_time   << "\n";
//---  rpt << "Time for worst-case error       : " << wc_time   << "\n";
//---  rpt << "Time for worst-case error (Chi) : " << wc_time_chi   << "\n";
//---  rpt << "Time for average-case error     : " << ac_time   << "\n";
//---  rpt << "Time for bit-flip error         : " << bf_time   << "\n";
//---  rpt << "Time for bit-flip error (Chi)   : " << bf_time_chi   << "\n";
//---}

} // namespace alice
//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
