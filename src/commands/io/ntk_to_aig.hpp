// Copyright (C) AGRA - University of Bremen
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : ntk_to_aig.hpp
// @brief  : converts an Old Network store to a AIG store (in Gia_Man_t*)
//
//------------------------------------------------------------------------------
#pragma once

#ifndef NTK_TO_AIG_HPP
#define NTK_TO_AIG_HPP

#define LIN64

#include <commands/axekit_stores.hpp>

namespace alice {

class ntk_to_aig_command : public command {
public:
  ntk_to_aig_command (const environment::ptr &env) : command (env, "Convert Ntk (Old style) to AIG")
  {
    opts.add_options()
      ( "id", po::value( &id )->default_value( id ), "store id of Network circuit" )
      ( "new,n" ,                                  "Create a new AIG store entry" )
      ;
  }

protected:
  rules_t validity_rules() const;
  bool execute(); 
  log_opt_t log() const;

private:
  unsigned id;
};

}

//------------------------------------------------------------------------------

#endif
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
