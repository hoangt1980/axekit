// Copyright (C) AGRA - University of Bremen
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : aiger_io.cpp
// @brief  : read and write aig
//
//------------------------------------------------------------------------------
#pragma once
#ifndef AIGER_IO_HPP
#define AIGER_IO_HPP

#define LIN64

#include <fstream>
#include <string>
#include <boost/regex.hpp>
#include <aig/gia/gia.h>
#include <boost/format.hpp>
#include <commands/axekit_stores.hpp>
#include <utils/common_utils.hpp>

//using namespace alice;

namespace alice {


class read_aiger_command : public command {
public:
  read_aiger_command (const environment::ptr &env) : command (env, "Read Aiger format to AxC")
  {
    add_positional_option ("filename");
    opts.add_options()
      ( "filename,f",   po::value (&file),         "The input Aiger file" )
      ( "store,s",      po::value( &store )->default_value( store ), "Which store to use? (Aig=0 / Ntk=2 (Old style))" )
      ( "new,n" ,                                  "Create a new store entry" )
      ;
  }

protected:
  rules_t validity_rules() const;
  // the main execution part of the command.
  bool execute(); 
  // Log for tool execution in JSON format
  log_opt_t log() const;

private:
  std::string file;
  int store = 0;
  float t_read = -1;
};



class write_aiger_command : public command {
public:
  write_aiger_command (const environment::ptr &env) : command (env, "Write the circuit in Aiger format")
  {
    add_positional_option ("filename");
    opts.add_options()
      ( "filename,f",   po::value (&file),         "The output Aiger file" )
      ( "store,s",      po::value( &store )->default_value( store ), "Which store to use? (Aig=0 / Ntk=2 (Old style))" )
      ( "id" ,          po::value( &id )->default_value( id ), "Store id of the network" )
      ;
  }

protected:
  rules_t validity_rules() const;
  // the main execution part of the command.
  bool execute(); 
  // Log for tool execution in JSON format
  log_opt_t log() const;

private:
  std::string file;
  int store = 0;
  int id = 0;
  float t_write = -1;
};



//---/* enable `read_aiger` for AIGs */
//---template<>
//---inline bool store_can_read_io_type <abc::Gia_Man_t*, io_aiger_tag_t> ( command& cmd ) {
//---  return true;
//---}
//---
//---/* implement `read_aiger` for AIGs */
//---template<>
//---inline abc::Gia_Man_t* store_read_io_type<abc::Gia_Man_t*, io_aiger_tag_t> (
//---  const std::string& filename, const command& cmd ) {
//---  return abc::Gia_AigerRead( (char*)filename.c_str(), 0, 0, 0 );
//---}
//---
//---/* enable `write_aiger` for AIGs */
//---template<>
//---inline bool store_can_write_io_type <abc::Gia_Man_t*, io_aiger_tag_t> ( command& cmd ) {
//---  return true;
//---}
//---
//---/* implement `write_aiger` for AIGs */
//---template<>
//---inline void store_write_io_type <abc::Gia_Man_t*, io_aiger_tag_t> (
//---  abc::Gia_Man_t* const& aig, const std::string& filename, const command& cmd ) {
//---  // use the opts here.
//---  abc::Gia_AigerWrite( aig, (char*)filename.c_str(), 1, 0 );
//---}
//---
//---
//---// stores/classical/
//---// store_can_convert () function
//---// store_can_convert (from, to) function return boolean
//---// store_convert () {}

  
}

#endif
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
