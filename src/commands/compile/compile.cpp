// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : compile.cpp
// @brief  : Approximate Synthesis
//           
//           
//------------------------------------------------------------------------------

#include "compile.hpp"

namespace alice
{

namespace {
// Private functions.

}

compile_command::rules_t compile_command::validity_rules() const {

  return {
    { [this]() {
	return store == 2;
      }, "Only Network supported currently. Use store 2 (Use conversion functions to change)" },


    { [this]() {
	if (2 == store) { // Network store
	  const auto& ntks = env->store<abc::Abc_Ntk_t*>();
	  return !ntks.empty();
	}
	else { // BDD or Gia store
	  return false;
	}
      }, "Empty store!" },

      
    { [this]() {
	if (2 == store) { // Network store
	  const auto& ntks = env->store<abc::Abc_Ntk_t*>();
	  return id < ntks.size() ;
	}
	else { // BDD or Gia store
	  return false;
	}
      }, "Store id out of range" },

      
    { [this]() {
	return is_set("simple") | is_set("random") | is_set("wc") | is_set("ac") | is_set("er_frac") | is_set("bf");
      }, "Specify at least 1 error-metric as input OR provide simple/random flag." }

  };

}


void compile_command::set_flags () {

  if ( is_set ("simple") ) return; // dont have to bother anything.
  
  if ( wc > 0 )     wc_flag = true;
  if ( bf > 0 )     bf_flag = true;
  if ( er_frac > 0 )     er_flag = true;
  if ( ac > 0 )     ac_flag = true;

  if (bf_flag && wc_flag) {
    bf_flag = false; wc_flag = false;
    wc_bf_flag = true;
  }

}


bool compile_command::execute() {
  set_flags ();
  assert (store == 2 && "Wrong store");
  if ( ! (is_set("simple") | is_set("random") | wc_bf_flag | wc_flag | bf_flag | er_flag) ) { // Not checking average-case for now.
    std::cout << "[e] Invalid error targets!. Exiting without synthesis\n";
    return true;
  }

  auto& ntks = env->store<abc::Abc_Ntk_t*> ();
  auto ntk = ntks[id];
  if (ntk == nullptr) {
    std::cout << "[e] Null in store! Exiting without synthesis\n";
    return false; 
  }
  ntk = abc::Abc_NtkStrash (ntk, 1, 1, 0); // Strash
  abc::Abc_AigCleanup((abc::Abc_Aig_t *)ntk->pManFunc); // Cleanup
  ntk = abc::Abc_NtkStrash (ntk, 1, 1, 0); // Strash again
  if (ntk == nullptr) {
    std::cout << "[e] Network reduced to null! Exiting without synthesis\n";
    return false; 
  }

  abc::Abc_Ntk_t* appx_ntk;
  std::tuple <float, float, float> res;  

  if ( is_set("simple") ) {
    appx_ntk = axekit::aig_first_cut ( ntk );
    res = axekit::from_aig_first_cut ();
  }
  else if ( is_set("random") ) {
    appx_ntk = axekit::aig_random_cut ( ntk );
    res = axekit::from_aig_random_cut ();
  }
  else if ( is_set("rewrite") ) {
    axekit::to_aig_rewrite1 ( er_frac, wc, ac, bf, wc_flag, ac_flag, er_flag, wc_bf_flag );
    axekit::to_aig_rewrite2 ( debug, 3, 10u, 3 );
    appx_ntk = axekit::aig_rewrite ( ntk );
    res = axekit::from_aig_rewrite ();
  }
  else {
    std::cout << "[e] Either --rewrite or --simple should be set. Exiting without synthesis\n";
    return false;
  }

  arw_sort_paths_time    = std::get<0> (res);
  arw_applycut_time      = std::get<1> (res);
  arw_generatecuts_time  = std::get<2> (res);
  
  // Process the appx_ntk and put it into the store.
  if (appx_ntk == nullptr) {
    std::cout << "[e] Approximation error. Exiting without synthesis\n";
    return false; 
  }
  appx_ntk = abc::Abc_NtkStrash (appx_ntk, 1, 1, 0); // Strash
  abc::Abc_AigCleanup((abc::Abc_Aig_t *)appx_ntk->pManFunc); // Cleanup
  appx_ntk = abc::Abc_NtkStrash (appx_ntk, 1, 1, 0); // Strash again
  if (appx_ntk == nullptr) {
    std::cout << "[e] Approximated result is null. Exiting without synthesis\n";
    return false; 
  }
  
  if ( is_set("new") )  ntks.extend();
  ntks.current() = appx_ntk;
  return true;
}



compile_command::log_opt_t compile_command::log() const  {
  return log_opt_t (
    {
      {"rpt_file", rpt_file},
      {"store_id", id},
      {"store_used", (store == 0u) ? std::string("BDD") : std::string("AIG") },
      {"algorithm_used",
	  is_set("simple") ? boost::lexical_cast<std::string>("aig_first_cut_rewriting")
	  : (
	    is_set("random") ? boost::lexical_cast<std::string>("aig_random_rewriting")
	    : boost::lexical_cast<std::string>("aig_rewriting")
	    )
	  },
      {"applied_error_rate_fraction",    boost::lexical_cast<std::string>(er_frac)},
      {"applied_worst_case_error",       boost::lexical_cast<std::string>(wc)},
      {"applied_average_case_error",     boost::lexical_cast<std::string>(ac)},
      {"applied_bit_flip_error",         boost::lexical_cast<std::string>(bf)},
      {"aig_rewrite_time_to_sort_paths", boost::lexical_cast<std::string> (arw_sort_paths_time) },
      {"aig_rewrite_time_to_apply_cuts", boost::lexical_cast<std::string> (arw_applycut_time) },
      {"aig_rewrite_time_to_generate_cuts", boost::lexical_cast<std::string> (arw_generatecuts_time) }
      }
    );
}


} // namespace alice
//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
