// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : report_error.cpp
// @brief  : 
//           
//           
//------------------------------------------------------------------------------

#include "report_error.hpp"
#include <fstream>
#include <utils/common_utils.hpp>
#include <boost/lexical_cast.hpp>

#include <iostream>
#include <ios>
#include <iomanip>

namespace alice
{

report_error_command::rules_t report_error_command::validity_rules()  const {	
  return {
    { [this]() {
	return store == 0u || store == 1u;
      }, "Only Aig/BDD supported currently. Use store = 1 or 0" },

      
    { [this]() {
	if (1u == store) { // BDD store
	  const auto& bdds = env->store<cirkit::bdd_function_t>();
	  return id1 < bdds.size() && id2 < bdds.size();
	}
	else if (0u == store) { // AIG store
	  auto& gias = env->store<abc::Gia_Man_t*> ();
	  return id1 < gias.size() && id2 < gias.size();	  
	}
	else { // Ntk store
	  return false;
	}
      }, "Store ids are out of range" },

    { [this]() {
	if ( !is_set("mc_exact") && !is_set("mc_approx") ) return true;
	return !( is_set ("mc_exact") && is_set ("mc_approx") );
      }, "SAT: conflicting options, use either --mc_exact or --mc_approx" },
      
      
    { [this]() {
	return is_set("wc") | is_set("wc_chi") |
	  is_set("ac") | is_set("er") | is_set("bf") | is_set("bf_chi") | is_set("all");
      }, "Specify which error-metric to compute. ( Use --all to compute all error-metrics )" }

  };
}


void report_error_command::set_flags () {
  clear_all();   
  if ( is_set ("wc") )     wc_flag = true;
  if ( is_set ("wc_chi") ) wc_chi_flag = true;
  if ( is_set ("bf") )     bf_flag = true;
  if ( is_set ("bf_chi") ) bf_chi_flag = true;
  if ( is_set ("er") )     er_flag = true;
  if ( is_set ("ac") )     ac_flag = true;

  if ( is_set ("all") )     all_flag = true;

  if (all_flag) {
    er_flag = true; ac_flag = true;
    wc_flag = true; wc_chi_flag = true;
    bf_flag = true; bf_chi_flag = true;
  }

}

bool report_error_command::execute() {
  set_flags();
  
  if (1u == store) { // BDD Store
    auto& bdds = env->store<cirkit::bdd_function_t> ();
    auto fs = bdds[id1];
    auto fshat = bdds[id2];

    // error-rate
    auto time_now = std::clock();
    if ( er_flag ) {
      error_rate = axekit::error_rate (fs, fshat);
      
      const auto num_inputs = fs.first.ReadSize();
      const mpuint one = 1;
      auto fr1 = axekit::mpuint_to_mpfloat (error_rate);
      auto fr2 = axekit::mpuint_to_mpfloat ( (one << num_inputs) );
      auto percent =  fr1/fr2 ;
      error_rate_percent = percent * 100;
      er_time = get_elapsed_time (time_now);
    }

    // worst-case
    if ( wc_flag ) {
      time_now = std::clock();
      wc_error = axekit::worst_case (fs, fshat); // normal
      wc_time = get_elapsed_time (time_now);
    }

    // worst-case-chi
    if ( wc_chi_flag ) {
      time_now = std::clock();
      wc_error_chi = axekit::worst_case_chi (fs, fshat); // characteristic (chi) function
      wc_time_chi = get_elapsed_time (time_now);
    }
    
    //avg-case
    if ( ac_flag ) {
      time_now = std::clock();
      avg_error = axekit::average_case (fs, fshat); 
      ac_time = get_elapsed_time (time_now);
    }
    
    //bit-flip
    if ( bf_flag ) {
      time_now = std::clock();
      bf_error = axekit::bit_flip_error (fs, fshat);
      bf_time = get_elapsed_time (time_now);
    }

    //bit-flip-chi
    if ( bf_chi_flag ) {
      time_now = std::clock();
      bf_error_chi = axekit::bit_flip_error_chi (fs, fshat);
      bf_time_chi = get_elapsed_time (time_now);
    }

    
  } // Upto here : BDD
  // GIA starts here.
  else if (0 == store) { // GIA
    wc_chi_flag = false;
    bf_chi_flag = false;

    auto& gias = env->store<abc::Gia_Man_t*> ();
    auto fs = gias[id1];
    auto fshat = gias[id2];

    // error-rate
    auto time_now = std::clock();
    if ( er_flag ) {
      if ( is_set ("mc_exact") )
	error_rate = axekit::error_rate_with_sharpsat (fs, fshat);
      else if ( is_set ("mc_approx") )
	error_rate = axekit::error_rate_with_cms_and_cnffile (fs, fshat);
      else { // switch based on num_inputs. 
	const auto num_inputs = abc::Gia_ManPiNum(fs);
	if (num_inputs > 128) {
	  std::cout << "[i] SAT: switching to approximate error-rate (#PI = "
		    << num_inputs << ")\n";
	  error_rate = axekit::error_rate_with_cms_and_cnffile (fs, fshat);
	}
	else
	  error_rate = axekit::error_rate_with_sharpsat (fs, fshat);
      }
      const auto num_inputs = abc::Gia_ManPiNum(fs);
      const mpuint one = 1;
      auto fr1 = axekit::mpuint_to_mpfloat (error_rate);
      auto fr2 = axekit::mpuint_to_mpfloat ( (one << num_inputs) );
      auto percent =  fr1/fr2 ;
      error_rate_percent = percent * 100;
      er_time = get_elapsed_time (time_now);
    }

    // worst-case
    if ( wc_flag ) {
      time_now = std::clock();
      wc_error = axekit::worst_case (fs, fshat);  
      wc_time = get_elapsed_time (time_now);
    }

    //avg-case
    if ( ac_flag ) {
      time_now = std::clock();
      if (aig_wc_threshold != 10000) {
	std::cout << "[w] SAT #Max-Value threshold changed. May result in longer runtime.\n";
      }
      //--native axc is buggy--if ( !is_set ("mc_approx") && !is_set("mc_exact") )
      //--native axc is buggy--	avg_error = axekit::average_case (fs, fshat, aig_wc_threshold, 0u);  // aXc internal.
      //--native axc is buggy--else if ( is_set ("mc_exact") )
      //--native axc is buggy--	avg_error = axekit::average_case (fs, fshat, aig_wc_threshold, 1u);  // sharpsat
      //--native axc is buggy--else if ( is_set ("mc_approx") ) {
      //--native axc is buggy--	std::cout << "[w] mc_approx in average-case error not yet supported. Reverting to aXc internal.\n";
      //--native axc is buggy--	//avg_error = axekit::average_case (fs, fshat, aig_wc_threshold, 2u);  // CMS
      //--native axc is buggy--	avg_error = axekit::average_case (fs, fshat, aig_wc_threshold, 0u);  // aXc internal.
      //--native axc is buggy--}
      //--native axc is buggy--else {
      //--native axc is buggy--	assert (false && "Cannot have both --mc_approx and --mc_exact set!");
      //--native axc is buggy--}

      avg_error = axekit::average_case (fs, fshat, aig_wc_threshold);  // by default it uses sharpsat.
      ac_time = get_elapsed_time (time_now);
    }
    
    
    //bit-flip
    if ( bf_flag ) {
      time_now = std::clock();
      bf_error = axekit::bit_flip_error (fs, fshat);
      bf_time = get_elapsed_time (time_now);
    }

  } // Upto here : GIA
  else {
    std::cout << "[e] Unknown store!\n";
    return false;
  }
  
  
  if(wc_flag)     std::cout << "[i] worst_case_error     = " << wc_error      << "\n";
  if(wc_chi_flag) std::cout << "[i] worst_case_error_chi = " << wc_error_chi  << "\n";
  if(ac_flag)     std::cout << "[i] average_case_error   = " << avg_error     << "\n";
  if(bf_flag)     std::cout << "[i] bit_flip_error       = " << bf_error      << "\n";
  if(bf_chi_flag) std::cout << "[i] bit_flip_error_chi   = " << bf_error_chi  << "\n";
  if(er_flag)     std::cout << "[i] error_rate           = " << error_rate;

  auto precision = std::cout.precision();
  if(er_flag)     std::cout << std::fixed << std::setprecision(2);
  if(er_flag)     std::cout << "  ( "  << error_rate_percent  << "  % ) "     << "\n";
  std::cout << std::setprecision(precision);
  std::cout << std::defaultfloat;
  
  write_report();
//  clear_all(); wrong...
  return true;
}



report_error_command::log_opt_t report_error_command::log() const  {
  return log_opt_t (
    {
      {"report", rpt_file},
      {"store_used", (store == 0u) ? std::string("AIG") : std::string("BDD") },
      {"SAT_wc_threshold_for_nMaxErrors", (store == 0u) ?
	  boost::lexical_cast<std::string>(aig_wc_threshold) : std::string("NA") },
      {"store_id1", id1 },
      {"store_id2", id2 },
      {"exact_error_rate (SharpSAT)", is_set("mc_exact") ?  "Yes" : "Default" },
      {"approx_error_rate (CMS)"    , is_set("mc_approx") ? "Yes" : "Default" },
      {"error_rate",           boost::lexical_cast<std::string>(error_rate)},
      {"time_error_rate",      boost::lexical_cast<std::string>(er_time)},
      {"worst_case_error",     boost::lexical_cast<std::string>(wc_error)},
      {"time_worst_case",      boost::lexical_cast<std::string>(wc_time)},
      {"worst_case_error_chi", boost::lexical_cast<std::string>(wc_error_chi)},
      {"time_worst_case_chi",  boost::lexical_cast<std::string>(wc_time_chi)},
      {"average_case_error",   boost::lexical_cast<std::string>(avg_error)},
      {"time_average_case",    boost::lexical_cast<std::string>(ac_time)},
      {"bit_flip_error",       boost::lexical_cast<std::string>(bf_error)},
      {"time_bit_flip",        boost::lexical_cast<std::string>(bf_time)},
      {"bit_flip_error_chi",   boost::lexical_cast<std::string>(bf_error_chi)},
      {"time_bit_flip_chi",    boost::lexical_cast<std::string>(bf_time_chi)}

      }
    );
}


void report_error_command::clear_all() {

  error_rate = 0u;   wc_error   = 0u;   wc_error_chi   = 0u;
  avg_error = 0;     bf_error = 0u;     bf_error_chi = 0u; 

  er_time = -1;   wc_time = -1;   wc_time_chi = -1;
  ac_time = -1;   bf_time = -1;   bf_time_chi = -1;

  wc_flag = false;   ac_flag = false;   wc_chi_flag = false;
  er_flag = false;   bf_flag = false;   bf_chi_flag = false;
  all_flag = false;

}

void report_error_command::write_report() {
  std::ofstream rpt (rpt_file);
  rpt << "AxC approximation error report  : " << axekit::curr_time() << "\n";
  rpt << "Report file                     : " << rpt_file   << "\n";
  rpt << "Error Count                     : " << error_rate << "\n";
  rpt << "Error Rate                      : " << error_rate << "\n";
  rpt << "Worst-Case Error                : " << wc_error  << "\n";
  rpt << "Worst-Case Error (Chi Method)   : " << wc_error_chi  << "\n";
  rpt << "Average-Case Error              : " << avg_error  << "\n";
  rpt << "Bit-Flip Error                  : " << bf_error  << "\n";
  rpt << "Bit-Flip Error (Chi Method)     : " << bf_error_chi  << "\n";
  rpt << "Time for error-rate             : " << er_time   << "\n";
  rpt << "Time for worst-case error       : " << wc_time   << "\n";
  rpt << "Time for worst-case error (Chi) : " << wc_time_chi   << "\n";
  rpt << "Time for average-case error     : " << ac_time   << "\n";
  rpt << "Time for bit-flip error         : " << bf_time   << "\n";
  rpt << "Time for bit-flip error (Chi)   : " << bf_time_chi   << "\n";
}

} // namespace alice
//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
