// Copyright (C) AGRA - University of Bremen
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : version.hpp
// @brief  : shows the tool-name and build
//
//------------------------------------------------------------------------------
#pragma once

#ifndef VERSION_HPP
#define VERSION_HPP

#include <iostream>
#include <string>

#define VSTRINGIFY(x) #x
#define VTOSTRING(x) VSTRINGIFY(x)
#ifndef GIT_COMMIT_HASH
#define GIT_COMMIT_HASH NA
#endif
#ifndef GIT_BRANCH
#define GIT_BRANCH NA
#endif

namespace alice {

class version_command : public command {
public:
  version_command (const environment::ptr &env) : command (env, "Show the build version")
  {
    opts.add_options()
      ;
  }
protected:
  // the main execution part of the command.
  bool execute() {
    ver = std::string ( VTOSTRING(GIT_BRANCH) ) + "-" +  std::string ( VTOSTRING(GIT_COMMIT_HASH) );
    std::cout << "AxC build: " << ver << "\n";
    return true;
  }
  // Log for tool execution in JSON format
  log_opt_t log() const {
    return log_opt_t (
      {
	{"axc_version", ver}
      }
      );
  }

private:
  std::string ver;
  
};

}

#endif
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
