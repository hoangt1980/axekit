// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : total_arithmetic_error.cpp
// @brief  : Sum of all errors (this divided by number of input combinations is avg-case)
//           
//           
//------------------------------------------------------------------------------

#include "total_arithmetic_error.hpp"
#include <fstream>
#include <utils/common_utils.hpp>
#include <boost/lexical_cast.hpp>

namespace alice
{

total_arithmetic_error_command::rules_t total_arithmetic_error_command::validity_rules() const {
  return {
    { [this]() {
	return (store == 1u  || store == 0u);
      }, "Only AIG/BDD supported currently. Use store = 0/1" },

      
    { [this]() {
	if (1u == store) { // BDD store
	  const auto& bdds = env->store<cirkit::bdd_function_t>();
	  return id < bdds.size();
	}
	if (0u == store) { // GIA store
	  const auto& gias = env->store<abc::Gia_Man_t*>();
	  return id < gias.size();
	}	
	else { // Network (old AIG) store
	  return false;
	}
      }, "Store id is out of range" }
  };
}

bool total_arithmetic_error_command::execute() {
  std::string warn = "";
  if (1u == store) {
    auto& bdds = env->store<cirkit::bdd_function_t> ();
    auto fs = bdds[id];

    auto time_now = std::clock();
    total_error = axekit::get_weighted_sum (fs);
    sum_time = get_elapsed_time (time_now);
      
  }
  else {
    auto& aigs = env->store<abc::Gia_Man_t*> ();
    auto fs = aigs[id];

    const auto num_max_vals = 100;
    auto time_now = std::clock();
    total_error = axekit::sum_of_max_values (fs, num_max_vals);
    sum_time = get_elapsed_time (time_now);
    warn = std::string (" (SAT #Max-Value threshold set to ") +
      std::to_string (num_max_vals) + ", with exact model counting)";

    //std::cout << "[i] total_arithmetic_error (sharpsat)  =  "
    // 	      << axekit::sum_of_max_values (fs, num_max_vals, 1) << warn << "\n";

  }
  std::cout << "[i] total_arithmetic_error (axc) =  " << total_error    << warn << "\n";
  return true;
}

total_arithmetic_error_command::log_opt_t total_arithmetic_error_command::log() const  {
  return log_opt_t (
    {
      {"store_used", (store == 1u) ? std::string("BDD") : std::string("AIG") },
      {"store_id", id },
      {"time_total_error",           boost::lexical_cast<std::string>(sum_time)},
      {"total_arithmetic_error",     boost::lexical_cast<std::string>(total_error)}
      }
    );
}


} // namespace alice
//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
